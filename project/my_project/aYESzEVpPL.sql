-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 17, 2019 at 04:47 AM
-- Server version: 8.0.13-4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aYESzEVpPL`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_emailaddress`
--

CREATE TABLE `account_emailaddress` (
  `id` int(11) NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `account_emailaddress`
--

INSERT INTO `account_emailaddress` (`id`, `email`, `verified`, `primary`, `user_id`) VALUES
(1, 'a01703013@itesm.mx', 1, 1, 1),
(2, 'sara.rodriguez98@asuncionqro.edu.mx', 0, 0, 1),
(3, 'supremebeautycs@gmail.com', 0, 1, 10),
(4, 'deny_lili@hotmail.com', 0, 0, 16),
(6, 'alejandro@mienvio.mx', 0, 0, 3),
(7, 'a01703171@itesm.mx', 1, 1, 9),
(8, 'gleasontrash@gmail.com', 0, 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `account_emailconfirmation`
--

CREATE TABLE `account_emailconfirmation` (
  `id` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `sent` datetime(6) DEFAULT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email_address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Appointment`
--

CREATE TABLE `Appointment` (
  `idAppointment` int(11) NOT NULL,
  `carnet` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Appointment`
--

INSERT INTO `Appointment` (`idAppointment`, `carnet`, `date`, `status`) VALUES
(38, 'a', '2019-10-09 06:00:00', 0),
(39, 'hola', '2019-10-22 00:00:00', 0),
(40, 'a', '2020-02-22 17:15:34', 0),
(44, 'a', '2019-10-26 00:00:00', 0),
(45, 'a', '2019-10-07 11:47:18', 0),
(46, 'hola', '2019-10-09 11:53:45', 0),
(47, 'a', '2019-10-07 12:07:12', 0),
(48, 'a', '2019-10-21 12:09:11', 0),
(49, 'a', '2019-10-07 12:13:38', 0),
(50, 'a', '2020-11-11 11:11:11', 0),
(51, 'a', '2019-10-07 12:15:53', 0),
(52, 'a', '2019-10-09 17:54:17', 0),
(53, 'a', '2020-01-21 17:54:40', 0),
(54, 'a', '2019-10-09 18:19:20', 0),
(55, 'a', '2019-08-20 21:27:03', 0),
(56, 'a', '2019-12-07 23:53:06', 0),
(57, 'a', '2019-09-05 11:28:23', 1),
(58, 'a', '2019-10-06 11:37:12', 0),
(59, 'a', '2019-10-12 16:27:01', 0),
(60, 'a', '2019-10-16 16:27:01', 0),
(61, 'a', '2019-10-10 17:43:21', 0),
(62, 'a', '2019-10-17 00:23:55', 0),
(63, 'a', '2019-10-13 09:27:22', 0),
(64, 'a', '2019-10-10 10:53:27', 0),
(65, 'a', '2019-10-19 10:52:49', 0),
(66, 'a', '2019-10-12 00:12:43', 0),
(67, 'a', '2019-10-10 11:11:48', 0),
(68, 'a', '2019-08-19 10:52:49', 1),
(69, 'a', '2019-08-20 10:52:49', 1),
(70, 'a', '2019-08-21 10:52:49', 1),
(71, 'a', '2019-10-15 11:35:32', 0),
(72, 'a', '2019-10-12 11:35:55', 0),
(73, 'a', '2019-10-09 15:47:28', 1),
(74, 'a', '2019-10-10 11:51:35', 1),
(75, 'a', '2019-10-09 19:00:38', 1),
(76, 'a', '2019-10-09 23:00:05', 1),
(77, 'a', '2019-10-09 16:09:28', 1),
(78, 'a', '2019-10-10 12:19:42', 1),
(79, 'a', '2019-10-11 12:23:19', 1),
(80, 'a', '2019-10-09 21:26:10', 1),
(81, 'a', '2019-10-10 18:30:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add service', 1, 'add_service'),
(2, 'Can change service', 1, 'change_service'),
(3, 'Can delete service', 1, 'delete_service'),
(4, 'Can add appoinments', 2, 'add_appoinments'),
(5, 'Can change appoinments', 2, 'change_appoinments'),
(6, 'Can delete appoinments', 2, 'delete_appoinments'),
(7, 'Can add carnet model', 3, 'add_carnetmodel'),
(8, 'Can change carnet model', 3, 'change_carnetmodel'),
(9, 'Can delete carnet model', 3, 'delete_carnetmodel'),
(10, 'Can add log entry', 4, 'add_logentry'),
(11, 'Can change log entry', 4, 'change_logentry'),
(12, 'Can delete log entry', 4, 'delete_logentry'),
(13, 'Can add group', 5, 'add_group'),
(14, 'Can change group', 5, 'change_group'),
(15, 'Can delete group', 5, 'delete_group'),
(16, 'Can add permission', 6, 'add_permission'),
(17, 'Can change permission', 6, 'change_permission'),
(18, 'Can delete permission', 6, 'delete_permission'),
(19, 'Can add user', 7, 'add_user'),
(20, 'Can change user', 7, 'change_user'),
(21, 'Can delete user', 7, 'delete_user'),
(22, 'Can add content type', 8, 'add_contenttype'),
(23, 'Can change content type', 8, 'change_contenttype'),
(24, 'Can delete content type', 8, 'delete_contenttype'),
(25, 'Can add session', 9, 'add_session'),
(26, 'Can change session', 9, 'change_session'),
(27, 'Can delete session', 9, 'delete_session'),
(28, 'Can add site', 10, 'add_site'),
(29, 'Can change site', 10, 'change_site'),
(30, 'Can delete site', 10, 'delete_site'),
(31, 'Can add association', 11, 'add_association'),
(32, 'Can change association', 11, 'change_association'),
(33, 'Can delete association', 11, 'delete_association'),
(34, 'Can add user social auth', 12, 'add_usersocialauth'),
(35, 'Can change user social auth', 12, 'change_usersocialauth'),
(36, 'Can delete user social auth', 12, 'delete_usersocialauth'),
(37, 'Can add code', 13, 'add_code'),
(38, 'Can change code', 13, 'change_code'),
(39, 'Can delete code', 13, 'delete_code'),
(40, 'Can add nonce', 14, 'add_nonce'),
(41, 'Can change nonce', 14, 'change_nonce'),
(42, 'Can delete nonce', 14, 'delete_nonce'),
(43, 'Can add partial', 15, 'add_partial'),
(44, 'Can change partial', 15, 'change_partial'),
(45, 'Can delete partial', 15, 'delete_partial'),
(46, 'Can add email confirmation', 16, 'add_emailconfirmation'),
(47, 'Can change email confirmation', 16, 'change_emailconfirmation'),
(48, 'Can delete email confirmation', 16, 'delete_emailconfirmation'),
(49, 'Can add email address', 17, 'add_emailaddress'),
(50, 'Can change email address', 17, 'change_emailaddress'),
(51, 'Can delete email address', 17, 'delete_emailaddress'),
(52, 'Can add social application', 18, 'add_socialapp'),
(53, 'Can change social application', 18, 'change_socialapp'),
(54, 'Can delete social application', 18, 'delete_socialapp'),
(55, 'Can add social application token', 19, 'add_socialtoken'),
(56, 'Can change social application token', 19, 'change_socialtoken'),
(57, 'Can delete social application token', 19, 'delete_socialtoken'),
(58, 'Can add social account', 20, 'add_socialaccount'),
(59, 'Can change social account', 20, 'change_socialaccount'),
(60, 'Can delete social account', 20, 'delete_socialaccount'),
(61, 'Can add periodic task', 21, 'add_periodictask'),
(62, 'Can change periodic task', 21, 'change_periodictask'),
(63, 'Can delete periodic task', 21, 'delete_periodictask'),
(64, 'Can add crontab', 22, 'add_crontabschedule'),
(65, 'Can change crontab', 22, 'change_crontabschedule'),
(66, 'Can delete crontab', 22, 'delete_crontabschedule'),
(67, 'Can add interval', 23, 'add_intervalschedule'),
(68, 'Can change interval', 23, 'change_intervalschedule'),
(69, 'Can delete interval', 23, 'delete_intervalschedule'),
(70, 'Can add periodic tasks', 24, 'add_periodictasks'),
(71, 'Can change periodic tasks', 24, 'change_periodictasks'),
(72, 'Can delete periodic tasks', 24, 'delete_periodictasks'),
(73, 'Can add task state', 25, 'add_taskmeta'),
(74, 'Can change task state', 25, 'change_taskmeta'),
(75, 'Can delete task state', 25, 'delete_taskmeta'),
(76, 'Can add saved group result', 26, 'add_tasksetmeta'),
(77, 'Can change saved group result', 26, 'change_tasksetmeta'),
(78, 'Can delete saved group result', 26, 'delete_tasksetmeta'),
(79, 'Can add worker', 27, 'add_workerstate'),
(80, 'Can change worker', 27, 'change_workerstate'),
(81, 'Can delete worker', 27, 'delete_workerstate'),
(82, 'Can add task', 28, 'add_taskstate'),
(83, 'Can change task', 28, 'change_taskstate'),
(84, 'Can delete task', 28, 'delete_taskstate'),
(85, 'Can add queue', 29, 'add_queue'),
(86, 'Can change queue', 29, 'change_queue'),
(87, 'Can delete queue', 29, 'delete_queue'),
(88, 'Can add message', 30, 'add_message'),
(89, 'Can change message', 30, 'change_message'),
(90, 'Can delete message', 30, 'delete_message'),
(91, 'Can add message', 31, 'add_message'),
(92, 'Can change message', 31, 'change_message'),
(93, 'Can delete message', 31, 'delete_message'),
(94, 'Can add queue', 32, 'add_queue'),
(95, 'Can change queue', 32, 'change_queue'),
(96, 'Can delete queue', 32, 'delete_queue');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) DEFAULT '0',
  `username` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `is_staff` tinyint(1) DEFAULT '0',
  `is_active` tinyint(1) DEFAULT '1',
  `date_joined` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$36000$JzMUgJPnZs5x$yrXnyHaixuuia22Ffk/ovGc5G9f/HU6Dz/31ocXa5PE=', '2019-10-10 15:43:01.136930', 0, 'a01703013', 'Test', 'User', 'a01703013@itesm.mx', 1, 1, '2019-10-10 15:42:47.640873'),
(2, '!eQQxfQcDcGriqtFdMDmrHv8UFOyIF5ZbrhDEMhAy', NULL, 0, 'IrvingPercam', '', '', '', 1, 1, '2019-10-10 16:03:19.304109'),
(3, '!9DZpS5tedaYrrZ1NcyfFyzpltfQ8vtVlxPZzkHZE', '2019-10-25 21:43:49.836727', 0, 'AlexGleasonMndz', 'Alejandro', 'Gleason', '', 0, 0, '2019-10-10 16:03:46.971814'),
(4, 'pbkdf2_sha256$36000$6Z4wLNMwbHMq$wld2XEaKueB+f3bCqzu7WL63lJvPH0sg1x1JpL9veDM=', '2019-10-10 23:08:46.324938', 1, 'alejandrogleason', '', '', 'gleasontrash@gmail.com', 1, 1, '2019-10-10 16:05:51.984292'),
(5, '!tFBO9ST3ZlQJudM6YwngQDQmIUGyMOyvTxj8flB0', '2019-10-10 23:01:13.506862', 0, 'AlexGleasonM', 'A', 'G •', '', 0, 1, '2019-10-10 23:01:08.916032'),
(6, '!3x9Gnk0UR2B0hsEVBcJLgJmCTumrgqEOngUEOrfe', '2019-11-15 22:47:17.462910', 0, 'alejandro', 'Alejandro', 'Gleason Méndez', '', 0, 1, '2019-10-10 23:10:43.816774'),
(7, '!NpxnNzKUsrWnpLs9llbgHejcLJJ3U5HxMBYRbZ2R', '2019-11-03 03:22:52.645670', 0, 'alejandro8', 'Alejandro', 'Gleason', '', 0, 1, '2019-10-10 23:11:55.015961'),
(8, 'pbkdf2_sha256$36000$Ur5so1ieB81m$UjR6QmMdohRTwKq0ucIDfOBKNuMVAauT+r8ivE3iWIE=', '2019-10-10 23:54:21.880912', 0, 'alegleasonms', '', '', 'alegleasonms@gmail.com', 0, 1, '2019-10-10 23:53:45.132855'),
(9, 'pbkdf2_sha256$36000$26QOI2v7Vnyk$bxO2Ihnb+cCzUpfuln3qMafpprsoCa2cV8b1RAQ42/Y=', '2019-11-12 03:01:20.965575', 1, 'irvs', 'Irving', 'Aguilar', 'a01703171@itesm.mx', 1, 1, '2019-10-11 20:04:04.995762'),
(10, '!9oLhE14dUYKKnhWtA5WgirsjkSOKo5KxsRDa7ee4', '2019-11-13 23:21:16.675612', 0, 'Alejandro Gleason', 'Alejandro', 'Gleason Méndez', 'supremebeautycs@gmail.com', 0, 1, '2019-10-16 04:19:12.128899'),
(11, 'pbkdf2_sha256$36000$CsHgnKHWfX5B$QZW4r3oI3GDwiIXrF9qbXqgUZW2wWAIMH/0pgnODscU=', '2019-10-21 23:18:57.940302', 0, 'Tony', '', '', 'apap71@gmail.com', 0, 1, '2019-10-21 23:18:43.288165'),
(12, 'pbkdf2_sha256$36000$T0pK9NQJ1woE$4XNJzTPmQuejQpaZ2dYQSD7+lAqJcf5TFHqdOny2pWg=', '2019-10-22 03:11:33.715798', 0, 'fily', '', '', 'filyvr1397@gmail.com', 0, 1, '2019-10-21 23:24:43.189698'),
(13, 'pbkdf2_sha256$36000$76AToBh2qPqT$WGIQZAYnaiODFzp1uXGoB9j2K84y4lqxxPK0rE1mEkQ=', '2019-10-24 23:06:36.128443', 0, 'dualipa', '', '', 'dualipa@gmail.com', 0, 1, '2019-10-24 23:05:21.488515'),
(14, 'pbkdf2_sha256$36000$3XN3f2IrTSXo$TtHvP9Yl2NxlzdQJ4IZ4EFlN7+K86DiUd5/XhH10agI=', '2019-10-28 01:14:09.439668', 0, 'Administrador', '', '', 'a012093245@itesm.mx', 1, 1, '2019-10-25 18:36:16.187643'),
(15, 'pbkdf2_sha256$36000$XuFLpLlN53RZ$G0bYrpSp/4130c1z8qVG9HKgSZXbf0hEJ34PA2PK9Ko=', '2019-11-04 00:32:54.592454', 0, 'dualipa2', '', '', 'sara.rodriguez98@asuncionqro.edu.mx', 0, 1, '2019-10-25 18:49:08.884786'),
(16, 'pbkdf2_sha256$36000$J5C9C53sRMnY$ewKtlUB+zfakfyypNVW/pgvJj9ETITnWe2ViVa/BC5E=', '2019-10-25 18:55:54.166245', 0, 'bilieelish', '', '', 'sara.rodriguez98@asuncionqro.edu.mx', 0, 1, '2019-10-25 18:55:36.509970'),
(17, '!g9Fd7b85BzOvoP44cVJ9MrkNyvVjFNuVNGmzbVMC', '2019-10-25 19:07:22.232015', 0, 'gleezy', 'Gleezy', 'M', '', 0, 1, '2019-10-25 19:07:17.110674'),
(30, 'pbkdf2_sha256$36000$CnmBeGPAhHKM$HYhQabEZ88YM5spBnvpDN8zGevET/lReqGYZaln5SVs=', NULL, 0, 'sara.rodrigueaadz98@asuncionqro.edu.mx', '', '', 'sara.rodrigueaadz98@asuncionqro.edu.mx', 0, 1, '2019-11-04 03:25:01.859155'),
(31, 'pbkdf2_sha256$36000$estuWIvNcDhu$MEl835iWtrzEOSp248EcW8p7EKHURzulBZ3MhKyZEhs=', NULL, 0, 'akex@gmail.com', '', '', 'akex@gmail.com', 0, 1, '2019-11-04 03:33:04.049651'),
(32, 'pbkdf2_sha256$36000$XcRxCA5gT9ZR$pxEArVWQyLVwc0Ah4JemrmR/u276AH8O4XACtEhEM9I=', '2019-11-04 03:37:05.712949', 0, 'aasskex@gmail.com', '', '', 'aasskex@gmail.com', 0, 1, '2019-11-04 03:36:24.932182'),
(33, 'pbkdf2_sha256$36000$aCWJi92ZTf0J$NNCYHix4RrGiFZSLeqqTSm0dL4n8ivh5lbOqzKjMBck=', '2019-11-15 22:54:28.646970', 1, 'alegleason3', '', '', 'ale@gmail.com', 1, 1, '2019-11-15 22:53:18.866957'),
(34, 'pbkdf2_sha256$36000$T5053c7vPmH7$9lsqCySl59GeOlSOoZL9RqtJidrUgWKzxVsMGdkbYTg=', NULL, 0, 'username', '', '', 'deny_liddli@hotmail.com', 0, 1, '2019-11-17 03:35:08.209713');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `celery_taskmeta`
--

CREATE TABLE `celery_taskmeta` (
  `id` int(11) NOT NULL,
  `task_id` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `result` longtext COLLATE utf8mb4_general_ci,
  `date_done` datetime(6) NOT NULL,
  `traceback` longtext COLLATE utf8mb4_general_ci,
  `hidden` tinyint(1) NOT NULL,
  `meta` longtext COLLATE utf8mb4_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `celery_tasksetmeta`
--

CREATE TABLE `celery_tasksetmeta` (
  `id` int(11) NOT NULL,
  `taskset_id` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `result` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `date_done` datetime(6) NOT NULL,
  `hidden` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext COLLATE utf8_unicode_ci,
  `object_repr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2019-10-10 16:34:46.240571', '2', 'http://localhost:8000', 1, '[{\"added\": {}}]', 10, 4),
(2, '2019-10-10 16:36:45.616015', '1', 'Google Login', 1, '[{\"added\": {}}]', 18, 4),
(3, '2019-10-10 16:38:30.237524', '2', 'Fb Login', 1, '[{\"added\": {}}]', 18, 4),
(4, '2019-10-10 23:09:44.883658', '1', 'example.com', 3, '', 10, 4),
(5, '2019-10-10 23:10:06.872387', '2', 'http://localhost:8000', 2, '[]', 10, 4),
(6, '2019-10-10 23:10:07.372619', '2', 'http://localhost:8000', 2, '[]', 10, 4),
(7, '2019-11-16 01:55:31.660858', '2', '0 0 * * * (m/h/d/dM/MY)', 1, '[{\"added\": {}}]', 22, 9),
(8, '2019-11-16 01:59:08.694117', '3', '5 * * * * (m/h/d/dM/MY)', 1, '[{\"added\": {}}]', 22, 9),
(9, '2019-11-16 02:00:57.265060', '2', 'Test: 5 * * * * (m/h/d/dM/MY)', 1, '[{\"added\": {}}]', 21, 9),
(10, '2019-11-16 02:07:01.848791', '2', 'Test: 5 * * * * (m/h/d/dM/MY)', 2, '[{\"changed\": {\"fields\": [\"task\"]}}]', 21, 9),
(11, '2019-11-16 02:30:43.313724', '2', 'Test: 5 * * * * (m/h/d/dM/MY)', 2, '[{\"changed\": {\"fields\": [\"regtask\", \"task\"]}}]', 21, 9),
(12, '2019-11-16 02:53:25.573013', '3', '55 * * * * (m/h/d/dM/MY)', 2, '[{\"changed\": {\"fields\": [\"minute\"]}}]', 22, 9),
(13, '2019-11-16 02:54:00.609090', '3', '57 * * * * (m/h/d/dM/MY)', 2, '[{\"changed\": {\"fields\": [\"minute\"]}}]', 22, 9),
(14, '2019-11-16 06:28:47.648437', '2', 'Test: 57 * * * * (m/h/d/dM/MY)', 2, '[{\"changed\": {\"fields\": [\"task\"]}}]', 21, 9),
(15, '2019-11-16 18:04:36.088166', '2', 'Test: 57 * * * * (m/h/d/dM/MY)', 2, '[{\"changed\": {\"fields\": [\"regtask\", \"task\"]}}]', 21, 9),
(16, '2019-11-17 02:24:44.873033', '2', 'Email Notifications: 0 0 * * * (m/h/d/dM/MY)', 2, '[{\"changed\": {\"fields\": [\"name\", \"crontab\"]}}]', 21, 9);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(17, 'account', 'emailaddress'),
(16, 'account', 'emailconfirmation'),
(4, 'admin', 'logentry'),
(5, 'auth', 'group'),
(6, 'auth', 'permission'),
(7, 'auth', 'user'),
(8, 'contenttypes', 'contenttype'),
(22, 'djcelery', 'crontabschedule'),
(23, 'djcelery', 'intervalschedule'),
(21, 'djcelery', 'periodictask'),
(24, 'djcelery', 'periodictasks'),
(25, 'djcelery', 'taskmeta'),
(26, 'djcelery', 'tasksetmeta'),
(28, 'djcelery', 'taskstate'),
(27, 'djcelery', 'workerstate'),
(30, 'djkombu', 'message'),
(29, 'djkombu', 'queue'),
(31, 'kombu_transport_django', 'message'),
(32, 'kombu_transport_django', 'queue'),
(9, 'sessions', 'session'),
(10, 'sites', 'site'),
(11, 'social_django', 'association'),
(13, 'social_django', 'code'),
(14, 'social_django', 'nonce'),
(15, 'social_django', 'partial'),
(12, 'social_django', 'usersocialauth'),
(20, 'socialaccount', 'socialaccount'),
(18, 'socialaccount', 'socialapp'),
(19, 'socialaccount', 'socialtoken'),
(2, 'users', 'appoinments'),
(3, 'users', 'carnetmodel'),
(1, 'users', 'service');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-10-10 15:38:11.769235'),
(2, 'auth', '0001_initial', '2019-10-10 15:38:27.342386'),
(3, 'account', '0001_initial', '2019-10-10 15:38:32.849929'),
(4, 'account', '0002_email_max_length', '2019-10-10 15:38:34.397190'),
(5, 'admin', '0001_initial', '2019-10-10 15:38:38.702997'),
(6, 'admin', '0002_logentry_remove_auto_add', '2019-10-10 15:38:40.059163'),
(7, 'contenttypes', '0002_remove_content_type_name', '2019-10-10 15:38:42.852550'),
(8, 'auth', '0002_alter_permission_name_max_length', '2019-10-10 15:38:44.406500'),
(9, 'auth', '0003_alter_user_email_max_length', '2019-10-10 15:38:46.052538'),
(10, 'auth', '0004_alter_user_username_opts', '2019-10-10 15:38:47.032515'),
(11, 'auth', '0005_alter_user_last_login_null', '2019-10-10 15:38:48.740194'),
(12, 'auth', '0006_require_contenttypes_0002', '2019-10-10 15:38:49.667325'),
(13, 'auth', '0007_alter_validators_add_error_messages', '2019-10-10 15:38:50.744741'),
(14, 'auth', '0008_alter_user_username_max_length', '2019-10-10 15:38:52.304321'),
(15, 'sessions', '0001_initial', '2019-10-10 15:38:54.723547'),
(16, 'sites', '0001_initial', '2019-10-10 15:38:56.827235'),
(17, 'sites', '0002_alter_domain_unique', '2019-10-10 15:38:58.113417'),
(18, 'default', '0001_initial', '2019-10-10 15:39:05.860193'),
(19, 'social_auth', '0001_initial', '2019-10-10 15:39:07.142379'),
(20, 'default', '0002_add_related_name', '2019-10-10 15:39:17.810390'),
(21, 'social_auth', '0002_add_related_name', '2019-10-10 15:39:18.879280'),
(22, 'default', '0003_alter_email_max_length', '2019-10-10 15:39:20.394184'),
(23, 'social_auth', '0003_alter_email_max_length', '2019-10-10 15:39:21.438129'),
(24, 'default', '0004_auto_20160423_0400', '2019-10-10 15:39:22.505299'),
(25, 'social_auth', '0004_auto_20160423_0400', '2019-10-10 15:39:23.541713'),
(26, 'social_auth', '0005_auto_20160727_2333', '2019-10-10 15:39:24.753057'),
(27, 'social_django', '0006_partial', '2019-10-10 15:39:27.240401'),
(28, 'social_django', '0007_code_timestamp', '2019-10-10 15:39:29.356148'),
(29, 'social_django', '0008_partial_timestamp', '2019-10-10 15:39:31.302293'),
(30, 'socialaccount', '0001_initial', '2019-10-10 15:39:42.001434'),
(31, 'socialaccount', '0002_token_max_lengths', '2019-10-10 15:39:43.692442'),
(32, 'socialaccount', '0003_extra_data_default_dict', '2019-10-10 15:39:45.029147'),
(33, 'users', '0001_initial', '2019-10-10 15:39:50.567656'),
(34, 'social_django', '0002_add_related_name', '2019-10-10 15:39:52.020815'),
(35, 'social_django', '0003_alter_email_max_length', '2019-10-10 15:39:53.067638'),
(36, 'social_django', '0001_initial', '2019-10-10 15:39:54.093986'),
(37, 'social_django', '0004_auto_20160423_0400', '2019-10-10 15:39:55.138994'),
(38, 'social_django', '0005_auto_20160727_2333', '2019-10-10 15:39:56.193759'),
(39, 'users', '0002_appoinments_nameservice', '2019-10-10 16:19:34.686942'),
(40, 'users', '0003_auto_20191010_1622', '2019-10-10 16:22:54.951354'),
(41, 'users', '0002_service_status', '2019-10-29 03:25:13.325763'),
(42, 'users', '0003_auto_20191104_0411', '2019-11-04 04:11:46.585133'),
(43, 'djcelery', '0001_initial', '2019-11-15 00:32:56.918106'),
(44, 'kombu_transport_django', '0001_initial', '2019-11-16 01:31:09.857105');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('07yyfeogr4hcyr45i51o0m8llurah5uu', 'NzNjNTJjMjdiN2E0OTcwMTJkNWUzMWE2M2VkMzY1ZTkzOGNjYmRhNTp7InR3aXR0ZXJ1bmF1dGhvcml6ZWRfdG9rZW5fbmFtZSI6WyJvYXV0aF90b2tlbj1ZZWt1d3dBQUFBQUJBT1lqQUFBQmJnUUNBbjQmb2F1dGhfdG9rZW5fc2VjcmV0PXduaGphdDljb1I0YnM2ak95NlJTRVhjc0NuemFXa1l3Jm9hdXRoX2NhbGxiYWNrX2NvbmZpcm1lZD10cnVlIiwib2F1dGhfdG9rZW49T0NhQ1dRQUFBQUFCQU9ZakFBQUJiZ1FDZGlBJm9hdXRoX3Rva2VuX3NlY3JldD1BbzJEeGlvRm9xRVZyTjVhRnJMeGMweFo4NlRLZXM0YyZvYXV0aF9jYWxsYmFja19jb25maXJtZWQ9dHJ1ZSJdLCJfYXV0aF91c2VyX2lkIjoiMTAiLCJzb2NpYWxhY2NvdW50X3N0YXRlIjpbeyJwcm9jZXNzIjoibG9naW4iLCJzY29wZSI6IiIsImF1dGhfcGFyYW1zIjoiIn0sIjhTMjFwS2phQkpJSiJdLCJzb2NpYWxfYXV0aF9sYXN0X2xvZ2luX2JhY2tlbmQiOiJsaW5rZWRpbi1vYXV0aDIiLCJjYXJuZXQiOiJob2xhIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMubGlua2VkaW4uTGlua2VkaW5PQXV0aDIiLCJ0d2l0dGVyX3N0YXRlIjoiME5ndVJTVmRiOEpsNk1FMVVoSVJkeThSOHhwc3lhelMiLCJsaW5rZWRpbi1vYXV0aDJfc3RhdGUiOiJwZThhZzMzUnZ5VVk5RmlDRFpwQTVwYVUzaDBSZTB0eSIsIl9hdXRoX3VzZXJfaGFzaCI6IjI2OGY2Y2JjYjJlMGNkZjYxOTViYTgwZWNkZmUwZDBiNzIwYTRkOWIifQ==', '2019-11-08 17:43:19.430324'),
('14y4u3rsv1ez9zjkvi4d9684ybs91d1a', 'OTIxMTU3NzY3MTlkMzc2YmRmNjI0NTc1YmVmMTQ2ODM5YmUyYzA5NDp7Il9hdXRoX3VzZXJfaWQiOiIxNyIsImFjY291bnRfdXNlciI6ImgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJhbGxhdXRoLmFjY291bnQuYXV0aF9iYWNrZW5kcy5BdXRoZW50aWNhdGlvbkJhY2tlbmQiLCJjYXJuZXQiOiJob2xhIiwiYWNjb3VudF92ZXJpZmllZF9lbWFpbCI6bnVsbCwiX2F1dGhfdXNlcl9oYXNoIjoiODg5OTk0MzZhYzIwMGQyZTk3MzBlZDVjNmRjODMwMWY4OGI4OTlmNSJ9', '2019-11-08 19:07:23.017957'),
('49maykl38au5wutj3npmb108c1ejlhgn', 'MTE3YTVkOGI3N2FjZTVlZTk1OTM5OGU2ZjJiNGEwOWVlODJhMjU2Yjp7Il9hdXRoX3VzZXJfaGFzaCI6IjZjZTU0ZjE1YzgxMGZkOGYxY2FiMWNhN2Q3YjcyZGE2MzhlNTRkNDAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMiJ9', '2019-11-05 03:11:34.476260'),
('8v3e5118nlf52lqaidxznf6q5du7ljlu', 'MTVlODNiZWJkMTZkMzNhOTNmMGY1ODg1YWNiNTFhYjI2NjI5MjllODp7ImNhcm5ldCI6ImhvbGEifQ==', '2019-11-18 01:53:17.202413'),
('a2v8v79bb38xkzyzcbj9eswc0kr6kcmd', 'NWE0YWM2NGQ1MGFmYmYyOTVmMzVjZjYxZTZjODc4YTgyMjU1Mjk1Mzp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiZ2l0aHViX3N0YXRlIjoiVVRKczJRalhlTFM0NlU1b0lkVmhsTU1yN2g1cmtQSGQiLCJjYXJuZXQiOiJob2xhIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiYWxsYXV0aC5hY2NvdW50LmF1dGhfYmFja2VuZHMuQXV0aGVudGljYXRpb25CYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYjg2NDcyMDcwNjdjZmY4MzQ3NmExNmFiNzVjMzk0MWRjNmE4Y2JjMyJ9', '2019-11-12 05:50:11.914930'),
('dpiokh0ngs0hf0mcc57ax4zrlpc62zji', 'MTVlODNiZWJkMTZkMzNhOTNmMGY1ODg1YWNiNTFhYjI2NjI5MjllODp7ImNhcm5ldCI6ImhvbGEifQ==', '2019-11-05 21:47:13.030805'),
('fomgx5yy2mqqt4ztp9io387n65ubd9f3', 'ZTE1MDcyZmUyYzVmOWI5YTZiZmE5YzUyZDQ3ZmFkNzJhMmU0ODFhODp7ImNhcm5ldCI6IiJ9', '2019-11-18 01:53:15.956603'),
('gd7kgt5tk9p6w05uak862thz3vug5l2i', 'MTNjYzJlZTA0ZTJmNzBiMzVjNjhhNmJlMzg1MDdjZjllMTkxNTRkMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmNlNTRmMTVjODEwZmQ4ZjFjYWIxY2E3ZDdiNzJkYTYzOGU1NGQ0MCIsImNhcm5ldCI6ImZpbHkiLCJfYXV0aF91c2VyX2lkIjoiMTIifQ==', '2019-11-04 23:25:51.890109'),
('gfciq9kd3n7fa53ps25k6aze3mtn6hj2', 'MTVlODNiZWJkMTZkMzNhOTNmMGY1ODg1YWNiNTFhYjI2NjI5MjllODp7ImNhcm5ldCI6ImhvbGEifQ==', '2019-11-17 03:15:29.415546'),
('gzsrlkkbztom5nedhyza0vunlwfpjlpn', 'YmUyNzFjYmRlODRjZjg2YzMxMmY5NmI5Y2M1Y2Q3ODJmZGMyNGMyZTp7Imdvb2dsZS1vYXV0aDJfc3RhdGUiOiIybjNaVkprd2IxSHp5SThNN0xSZjZyd1BHYkhHTEx1YSIsImNhcm5ldCI6bnVsbCwibmV4dCI6Ii9wcm9maWxlLyJ9', '2019-12-01 03:35:11.629710'),
('iko72hagj2zp3q00jztcihp1ycgdd9zf', 'ZDNjMzlkZTFmNTUyZjMzOGRlYTVlMGNiNDI3ZGYyYjRkNGYyNzgwYjp7Il9hdXRoX3VzZXJfaWQiOiIxMCIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImxpbmtlZGluLW9hdXRoMiIsImNhcm5ldCI6ImhvbGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5saW5rZWRpbi5MaW5rZWRpbk9BdXRoMiIsImxpbmtlZGluLW9hdXRoMl9zdGF0ZSI6ImNnbk5mRlJQM3E2b2xtNmxGREhNaVV5NFJlWDR3VGE5IiwiX2F1dGhfdXNlcl9oYXNoIjoiMjY4ZjZjYmNiMmUwY2RmNjE5NWJhODBlY2RmZTBkMGI3MjBhNGQ5YiJ9', '2019-11-05 21:47:32.570312'),
('iopv37yovhtqsbchvxvetdbh5y6s1f1z', 'OTUwMTNlZDdlNTM2MDE5OTJiYzRlNDA1MDI2YTkzM2MyOTBhMDVkMjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiM2UxZTJhODgxZGE2ZmI5ZTI1YTA4ZWJlM2M5YTZlNmNhZGU4ZTM4NyIsImNhcm5ldCI6ImhvbGEiLCJfYXV0aF91c2VyX2lkIjoiMTMifQ==', '2019-11-07 23:06:36.878940'),
('jkwit3254jd7m9dqq4uoz993zn90y3u2', 'NjU0MTEzZmI4ZjQ1ZWM5ZTM5YTA3ZTQzOWE5MTE5MmQyNzliODUzYzp7InR3aXR0ZXJfc3RhdGUiOiJ1SjNtNzVsVGhSOFo2aG0wMHQxQ252MTZRbDJrN1VUNiIsInR3aXR0ZXJ1bmF1dGhvcml6ZWRfdG9rZW5fbmFtZSI6WyJvYXV0aF90b2tlbj1MbmRUUndBQUFBQUJBT1lqQUFBQmJpOGFFSkUmb2F1dGhfdG9rZW5fc2VjcmV0PUdLNVJXcHE1UXM3Y2t1VVhQbmxSOEYycktTb1dSa21RJm9hdXRoX2NhbGxiYWNrX2NvbmZpcm1lZD10cnVlIl0sImNhcm5ldCI6ImhvbGEifQ==', '2019-11-17 02:36:33.179336'),
('kggna1wh7wy157k0jmgiaa2yv9r9tjkg', 'YTM3NTIzZjI1MjlmN2RmYjgyYTljYWY5ODE5MGVjZmI3MzhlYzYzZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzkyNDk4Zjk4YTkyMWJhNjNmMDg3M2NjZTViMzZjOTY5ODQwYjBkMiIsImNhcm5ldCI6ImhvbGEiLCJfYXV0aF91c2VyX2lkIjoiMzMiLCJfcGFzc3dvcmRfcmVzZXRfa2V5IjoiNWJnLWUwOGUyNDM1ZWMyMjJiN2QzYWRlIn0=', '2019-11-29 22:54:29.302730'),
('kz3er8cdc30xuo6vefp2d292bebto2xe', 'YjA1ZWVkZjMzM2NjYzEzYmRkMDI3ZjdhMDM4YTQ5ZDRjMWNiYTFiODp7Il9hdXRoX3VzZXJfaWQiOiIxMCIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImxpbmtlZGluLW9hdXRoMiIsImNhcm5ldCI6ImhvbGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5saW5rZWRpbi5MaW5rZWRpbk9BdXRoMiIsImxpbmtlZGluLW9hdXRoMl9zdGF0ZSI6InpmV0dYandhdTBSYlRQQ2xQdTI2eWpHdndLeG9wWUlZIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjY4ZjZjYmNiMmUwY2RmNjE5NWJhODBlY2RmZTBkMGI3MjBhNGQ5YiJ9', '2019-11-06 17:02:49.127300'),
('kzvs580955qd7hgy6fw484qb0samn98t', 'ZmE2NDAwZWFkYmE3YWMwODI4NTliNWMyZDM1OTRjOWRiOTE0YzIxODp7Il9hdXRoX3VzZXJfaWQiOiIxMCIsInNvY2lhbF9hdXRoX2xhc3RfbG9naW5fYmFja2VuZCI6ImxpbmtlZGluLW9hdXRoMiIsImNhcm5ldCI6ImhvbGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJzb2NpYWxfY29yZS5iYWNrZW5kcy5saW5rZWRpbi5MaW5rZWRpbk9BdXRoMiIsImxpbmtlZGluLW9hdXRoMl9zdGF0ZSI6ImdmbTU0eUUyWFFsZEZUVHdEVzRkTUN6d2lJdjNGcElUIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjY4ZjZjYmNiMmUwY2RmNjE5NWJhODBlY2RmZTBkMGI3MjBhNGQ5YiJ9', '2019-11-05 12:30:15.103078'),
('lgar6onamm1sme8zbckxf2o2il1t3545', 'YjkyNGQ5ZWJmODYyMzNhNDIzMTI4YWUwZDE5ZTQxMjI0OWQyMzFhNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI0YzlkMzM2NjliOTNlZDVmMDc5MDczZmFhZjlhOGVjODllODI0NCIsImNhcm5ldCI6ImhvbGEiLCJfYXV0aF91c2VyX2lkIjoiOSJ9', '2019-10-25 21:59:17.494064'),
('m3jm3ybr4t45z31zhhx2cnziyfh80cxy', 'MTVlODNiZWJkMTZkMzNhOTNmMGY1ODg1YWNiNTFhYjI2NjI5MjllODp7ImNhcm5ldCI6ImhvbGEifQ==', '2019-11-18 01:20:17.369037'),
('m7lpsxh3qux4cewooz318y0x8c1fcak8', 'MmVlNTI3ODUyOWJhZjlmOTY5NzQ3OTZlMjNiN2U3YWQwYmEzYTU2Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI0YzlkMzM2NjliOTNlZDVmMDc5MDczZmFhZjlhOGVjODllODI0NCIsImNhcm5ldCI6ImlydmNhcm5ldCIsIl9hdXRoX3VzZXJfaWQiOiI5In0=', '2019-11-26 03:01:21.637648'),
('omou30p948rtuid0qfeamij3oifp5x1q', 'MTVlODNiZWJkMTZkMzNhOTNmMGY1ODg1YWNiNTFhYjI2NjI5MjllODp7ImNhcm5ldCI6ImhvbGEifQ==', '2019-11-05 21:47:11.580619'),
('tufpopsw8cacp23r0qvkvd4bwcn944di', 'MTVlODNiZWJkMTZkMzNhOTNmMGY1ODg1YWNiNTFhYjI2NjI5MjllODp7ImNhcm5ldCI6ImhvbGEifQ==', '2019-11-18 01:48:17.869485'),
('u6hnwepsgvcae7fto8d2s01nwvgl1pp0', 'YjkyNGQ5ZWJmODYyMzNhNDIzMTI4YWUwZDE5ZTQxMjI0OWQyMzFhNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI0YzlkMzM2NjliOTNlZDVmMDc5MDczZmFhZjlhOGVjODllODI0NCIsImNhcm5ldCI6ImhvbGEiLCJfYXV0aF91c2VyX2lkIjoiOSJ9', '2019-10-27 19:16:02.688345'),
('y6uq3evqkqz2qld241a3juracvy110ty', 'M2NlNjFhNDY3ZmQ1Yzc5ZTdlZGRjZWVmZDJiZWY4MDlmNDAyYTMxMDp7Il9hdXRoX3VzZXJfaWQiOiIzIiwic29jaWFsX2F1dGhfbGFzdF9sb2dpbl9iYWNrZW5kIjoiZ2l0aHViIiwiZ2l0aHViX3N0YXRlIjoiRjFaU084MXV0UHJHbXVmSENLWHlOWnp0b1RHRUROR3UiLCJjYXJuZXQiOiJob2xhIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoic29jaWFsX2NvcmUuYmFja2VuZHMuZ2l0aHViLkdpdGh1Yk9BdXRoMiIsIl9hdXRoX3VzZXJfaGFzaCI6IjczNjMyZTkxN2U4Mzg1MDRkOTZkZGU4ODY2YmVkODFkZTI5NjRmZTIifQ==', '2019-11-06 17:04:28.788248');

-- --------------------------------------------------------

--
-- Table structure for table `django_site`
--

CREATE TABLE `django_site` (
  `id` int(11) NOT NULL,
  `domain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(2, 'http://localhost:8000', 'http://localhost:8000');

-- --------------------------------------------------------

--
-- Table structure for table `djcelery_crontabschedule`
--

CREATE TABLE `djcelery_crontabschedule` (
  `id` int(11) NOT NULL,
  `minute` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `hour` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `day_of_week` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `day_of_month` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `month_of_year` varchar(64) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `djcelery_crontabschedule`
--

INSERT INTO `djcelery_crontabschedule` (`id`, `minute`, `hour`, `day_of_week`, `day_of_month`, `month_of_year`) VALUES
(1, '0', '4', '*', '*', '*'),
(2, '0', '0', '*', '*', '*'),
(3, '57', '*', '*', '*', '*');

-- --------------------------------------------------------

--
-- Table structure for table `djcelery_intervalschedule`
--

CREATE TABLE `djcelery_intervalschedule` (
  `id` int(11) NOT NULL,
  `every` int(11) NOT NULL,
  `period` varchar(24) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djcelery_periodictask`
--

CREATE TABLE `djcelery_periodictask` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `task` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `args` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `kwargs` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `queue` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `exchange` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `routing_key` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `expires` datetime(6) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_run_at` datetime(6) DEFAULT NULL,
  `total_run_count` int(10) UNSIGNED NOT NULL,
  `date_changed` datetime(6) NOT NULL,
  `description` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `crontab_id` int(11) DEFAULT NULL,
  `interval_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `djcelery_periodictask`
--

INSERT INTO `djcelery_periodictask` (`id`, `name`, `task`, `args`, `kwargs`, `queue`, `exchange`, `routing_key`, `expires`, `enabled`, `last_run_at`, `total_run_count`, `date_changed`, `description`, `crontab_id`, `interval_id`) VALUES
(1, 'celery.backend_cleanup', 'celery.backend_cleanup', '[]', '{}', NULL, NULL, NULL, NULL, 1, NULL, 0, '2019-11-16 18:09:17.572808', '', 1, NULL),
(2, 'Email Notifications', 'my_project.tasks.email_appointment_notification', '[]', '{}', NULL, NULL, NULL, NULL, 1, '2019-11-16 18:09:19.021108', 4, '2019-11-17 02:24:44.696408', '', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `djcelery_periodictasks`
--

CREATE TABLE `djcelery_periodictasks` (
  `ident` smallint(6) NOT NULL,
  `last_update` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `djcelery_periodictasks`
--

INSERT INTO `djcelery_periodictasks` (`ident`, `last_update`) VALUES
(1, '2019-11-17 02:24:44.300867');

-- --------------------------------------------------------

--
-- Table structure for table `djcelery_taskstate`
--

CREATE TABLE `djcelery_taskstate` (
  `id` int(11) NOT NULL,
  `state` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `task_id` varchar(36) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `tstamp` datetime(6) NOT NULL,
  `args` longtext COLLATE utf8mb4_general_ci,
  `kwargs` longtext COLLATE utf8mb4_general_ci,
  `eta` datetime(6) DEFAULT NULL,
  `expires` datetime(6) DEFAULT NULL,
  `result` longtext COLLATE utf8mb4_general_ci,
  `traceback` longtext COLLATE utf8mb4_general_ci,
  `runtime` double DEFAULT NULL,
  `retries` int(11) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `worker_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djcelery_workerstate`
--

CREATE TABLE `djcelery_workerstate` (
  `id` int(11) NOT NULL,
  `hostname` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `last_heartbeat` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `djkombu_message`
--

CREATE TABLE `djkombu_message` (
  `id` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `sent_at` datetime(6) DEFAULT NULL,
  `payload` longtext COLLATE utf8mb4_general_ci NOT NULL,
  `queue_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `djkombu_message`
--

INSERT INTO `djkombu_message` (`id`, `visible`, `sent_at`, `payload`, `queue_id`) VALUES
(1, 0, '2019-11-16 02:05:01.359243', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDA4ZDljOTNmLWQyYTItNDk1Zi1hNTZiLTQ4YzM4YjMxYWZjOXELVQdyZXRyaWVzcQxLAFUEdGFza3ENWCcAAABwcm9qZWN0Lm15X3Byb2plY3QubXlfcHJvamVjdC50YXNrcy5qb2JxDlUJdGltZWxpbWl0cQ9OToZVA2V0YXEQTlUGa3dhcmdzcRF9cRJ1Lg==\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"08d9c93f-d2a2-495f-a56b-48c38b31afc9\", \"reply_to\": \"09d4a212-1d1c-38aa-ac22-bb8dfb378e18\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"592ef584-8f69-4df6-9fb1-499aa5cc18ba\"}, \"content-encoding\": \"binary\"}', 1),
(2, 0, '2019-11-16 02:39:57.045802', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDRkZGJlYjk4LTUwYzEtNGQyOC04YTVmLWFhODRiYWIwNmQyN3ELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"4ddbeb98-50c1-4d28-8a5f-aa84bab06d27\", \"reply_to\": \"bb24c1a0-470a-352b-9068-2e62c2529141\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"e6fae071-cc71-4dbd-9efa-a11367512393\"}, \"content-encoding\": \"binary\"}', 1),
(3, 0, '2019-11-16 02:40:07.458826', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDc3ZjNjMTg1LWJjZTMtNDY5Yy04NTg0LTg1YTMyNzlmNWIxMnELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"77f3c185-bce3-469c-8584-85a3279f5b12\", \"reply_to\": \"e92d92cb-456d-3c98-a478-1ca03add2748\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"093a73ef-ba63-482b-a508-8873440f71f9\"}, \"content-encoding\": \"binary\"}', 1),
(4, 0, '2019-11-16 02:44:05.383097', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJGU4ODliM2FmLWZhOGYtNGE5ZC1hZWUwLWUwZTE2NGYyMTMxZHELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"e889b3af-fa8f-4a9d-aee0-e0e164f2131d\", \"reply_to\": \"ef28b9cd-b4fc-384d-aac9-20d138f1c3e5\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"b4c1334d-1613-405b-beee-e4781359ed63\"}, \"content-encoding\": \"binary\"}', 1),
(5, 0, '2019-11-16 02:54:43.478351', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJGFjMmQ4ODdkLWU5YmEtNGVmZS05MmVhLWQ2MjA1MWJlNWI4ZXELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"ac2d887d-e9ba-4efe-92ea-d62051be5b8e\", \"reply_to\": \"f1552714-e3fb-3c63-86dc-0381b28cf893\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"5be97e46-61cb-459a-94fa-e7e91f8372cc\"}, \"content-encoding\": \"binary\"}', 1),
(6, 0, '2019-11-16 02:57:01.427198', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDQ3NjY0MWM2LTYzNDEtNDU2NC05Mzk3LWM4NjkzZWI5MmQ3ZHELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"476641c6-6341-4564-9397-c8693eb92d7d\", \"reply_to\": \"f1552714-e3fb-3c63-86dc-0381b28cf893\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"ee68963c-6241-436f-8838-4dd4cd7c8c7f\"}, \"content-encoding\": \"binary\"}', 1),
(7, 0, '2019-11-16 06:00:31.605034', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDY3MjUwODU4LWYxNWYtNDQ4YS04Njc3LTRiMjYzNWRmOGU4YnELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"67250858-f15f-448a-8677-4b2635df8e8b\", \"reply_to\": \"61d0a321-3dfe-3b88-afc7-886f0192ac8d\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"5b4f56cb-c1e7-451d-87b2-4a4195da5638\"}, \"content-encoding\": \"binary\"}', 1),
(8, 0, '2019-11-16 06:00:35.424998', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDFkM2E0YTFiLTRjMTktNGFmMC05OTJiLWFjNTk4ODEzODIwM3ELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"1d3a4a1b-4c19-4af0-992b-ac5988138203\", \"reply_to\": \"bed95799-e737-3f5c-805c-b625b2fddc2e\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"ad94e5ab-8702-441d-ac2a-399e1a60d4f1\"}, \"content-encoding\": \"binary\"}', 1),
(9, 0, '2019-11-16 06:03:58.643254', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJGQ5NjRmYmY0LTY5NGYtNDQzNS05MTE1LTYwN2I2NjBmNWRhMXELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"d964fbf4-694f-4435-9115-607b660f5da1\", \"reply_to\": \"9a0f2d90-6254-3325-8bdf-43d16b293da9\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"55ff00ed-e00d-4e79-9435-85f90f788c9c\"}, \"content-encoding\": \"binary\"}', 1),
(10, 0, '2019-11-16 06:05:36.699106', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDU1OTE1NzE1LTZlZDYtNDUwMS1hMzYyLTE0M2MwMzNlOTJkNnELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"55915715-6ed6-4501-a362-143c033e92d6\", \"reply_to\": \"7cbc89eb-00ad-3a7d-aeb9-0f46edca603d\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"c22e7755-7f4e-4428-97a6-d63894838e49\"}, \"content-encoding\": \"binary\"}', 1),
(11, 0, '2019-11-16 06:11:03.491026', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJGFmOGNjMzA3LTZhNmUtNDkwNy04MDFlLTVhMGI3ZjA4NWFjZHELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"af8cc307-6a6e-4907-801e-5a0b7f085acd\", \"reply_to\": \"d4f166a6-3031-31be-bec8-414ad5fe5bc9\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"dc96643b-3381-408f-8a40-7766b3714320\"}, \"content-encoding\": \"binary\"}', 1),
(12, 0, '2019-11-16 06:13:06.869224', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDU2MjY1NTdmLTgzNjUtNGE5ZS05YzQyLTUwYTA0YjI3YTEwOXELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"5626557f-8365-4a9e-9c42-50a04b27a109\", \"reply_to\": \"417800ff-2350-3955-942d-ddf34d8085db\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"12a0a1a3-8b66-4701-bb35-c440e006dce9\"}, \"content-encoding\": \"binary\"}', 1),
(13, 0, '2019-11-16 06:14:02.547514', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJGI5ODQyOTZkLWJiMjEtNDA3Yy1iODJhLTg0ZWMyY2I0MmNmOXELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRdteV9wcm9qZWN0LmNlbGVyeS5oZWxsb3EOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"b984296d-bb21-407c-b82a-84ec2cb42cf9\", \"reply_to\": \"f419c91c-db10-3eeb-a988-1a229c94844f\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"36e93aa9-1c90-496c-af7b-7879bde16128\"}, \"content-encoding\": \"binary\"}', 1),
(14, 0, '2019-11-16 06:22:14.907300', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDRlMmQwZTMyLTNmMGQtNGM2MC1hMWE1LWQ5YjIyODEzNTBiOXELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRRteV9wcm9qZWN0LnRhc2tzLmpvYnEOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"4e2d0e32-3f0d-4c60-a1a5-d9b2281350b9\", \"reply_to\": \"d2e3deff-fc0c-30b5-8287-45a287555b81\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"38f55ca0-b4a9-4ec9-af7c-45adbf639a09\"}, \"content-encoding\": \"binary\"}', 1),
(15, 0, '2019-11-16 06:28:00.006868', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJGZmMGM2MWUyLWU0YzQtNGFlYS1iNGU0LTM4NzU1NjMxMTM4Y3ELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRRteV9wcm9qZWN0LnRhc2tzLmpvYnEOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"ff0c61e2-e4c4-4aea-b4e4-38755631138c\", \"reply_to\": \"5130314f-47ca-3f63-8243-d886688a0bf8\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"a3474196-3908-4498-acf7-29b3dbcfc83d\"}, \"content-encoding\": \"binary\"}', 1),
(16, 0, '2019-11-16 06:29:22.673029', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDY2NDM0NjdjLWNmZmMtNGM4Yy05OWM4LWZiYWQwNDM0MzEwNXELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRRteV9wcm9qZWN0LnRhc2tzLmpvYnEOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"6643467c-cffc-4c8c-99c8-fbad04343105\", \"reply_to\": \"ecc50026-bc5b-3e08-abe9-40fb0b6e95aa\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"813be0a5-abd4-4204-869f-cbb51364bcde\"}, \"content-encoding\": \"binary\"}', 1),
(17, 0, '2019-11-16 06:29:28.393689', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDkxY2Q2ZDA2LTI2M2EtNDhmOC05YTk0LTA2MmMxNzI5ZGQ3NXELVQdyZXRyaWVzcQxLAFUEdGFza3ENVRRteV9wcm9qZWN0LnRhc2tzLmpvYnEOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"91cd6d06-263a-48f8-9a94-062c1729dd75\", \"reply_to\": \"ecc50026-bc5b-3e08-abe9-40fb0b6e95aa\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"bc9eeb48-a6ca-4c8f-95d6-dab440269624\"}, \"content-encoding\": \"binary\"}', 1),
(18, 0, '2019-11-16 18:06:12.833342', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJGQ3OTAyOGFlLTIxYWUtNDhjMS1iYmUxLWEzZmZiOTA0ZWUyNnELVQdyZXRyaWVzcQxLAFUEdGFza3ENVS9teV9wcm9qZWN0LnRhc2tzLmVtYWlsX2FwcG9pbnRtZW50X25vdGlmaWNhdGlvbnEOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"d79028ae-21ae-48c1-bbe1-a3ffb904ee26\", \"reply_to\": \"50dc5d16-3cbd-3493-97e3-b7eb359cb0db\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"d751ae3c-ace9-4db1-a951-2f0787e7f81e\"}, \"content-encoding\": \"binary\"}', 1),
(19, 0, '2019-11-16 18:09:12.962786', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDljMGEwZmU1LWVkMGYtNGYzOC1hOTI4LTlhOTNlNmIxOTRlOXELVQdyZXRyaWVzcQxLAFUEdGFza3ENVS9teV9wcm9qZWN0LnRhc2tzLmVtYWlsX2FwcG9pbnRtZW50X25vdGlmaWNhdGlvbnEOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"9c0a0fe5-ed0f-4f38-a928-9a93e6b194e9\", \"reply_to\": \"bd184889-c320-323e-bbd7-2cdc2a437943\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"ec068e42-8386-4a53-99b2-71bff67ea5d3\"}, \"content-encoding\": \"binary\"}', 1),
(20, 0, '2019-11-16 18:09:20.188836', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDQ5MDBlZTE1LTZkMTItNDE2ZC1hZGQ1LTc5OWY4M2ZlZGFiOXELVQdyZXRyaWVzcQxLAFUEdGFza3ENVS9teV9wcm9qZWN0LnRhc2tzLmVtYWlsX2FwcG9pbnRtZW50X25vdGlmaWNhdGlvbnEOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"4900ee15-6d12-416d-add5-799f83fedab9\", \"reply_to\": \"bd184889-c320-323e-bbd7-2cdc2a437943\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"888f12e6-e6d3-4f49-8ec7-ab460e7cf43a\"}, \"content-encoding\": \"binary\"}', 1),
(21, 1, '2019-11-17 02:24:14.582421', '{\"body\": \"gAJ9cQEoVQdleHBpcmVzcQJOVQN1dGNxA4hVBGFyZ3NxBF1xBVUFY2hvcmRxBk5VCWNhbGxiYWNrc3EHTlUIZXJyYmFja3NxCE5VB3Rhc2tzZXRxCU5VAmlkcQpVJDgyMzFiZTg4LWE1YWEtNDRiZS04ZWYxLWFmMmIxMDhhYmYwY3ELVQdyZXRyaWVzcQxLAFUEdGFza3ENVS9teV9wcm9qZWN0LnRhc2tzLmVtYWlsX2FwcG9pbnRtZW50X25vdGlmaWNhdGlvbnEOVQl0aW1lbGltaXRxD05OhlUDZXRhcRBOVQZrd2FyZ3NxEX1xEnUu\", \"headers\": {}, \"content-type\": \"application/x-python-serialize\", \"properties\": {\"body_encoding\": \"base64\", \"correlation_id\": \"8231be88-a5aa-44be-8ef1-af2b108abf0c\", \"reply_to\": \"336f694b-a8c5-393c-a351-17d4b0accdd8\", \"delivery_info\": {\"priority\": 0, \"routing_key\": \"celery\", \"exchange\": \"celery\"}, \"delivery_mode\": 2, \"delivery_tag\": \"00dfcd68-be17-49fb-b557-f0b862c73df6\"}, \"content-encoding\": \"binary\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `djkombu_queue`
--

CREATE TABLE `djkombu_queue` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `djkombu_queue`
--

INSERT INTO `djkombu_queue` (`id`, `name`) VALUES
(1, 'celery'),
(2, 'celery@MacBook-Pro-de-Irving.local.celery.pidbox');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `plataform` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `title`, `content`, `plataform`) VALUES
(1, 'pruea', 'hshjs', 'android'),
(2, 'test', 'this is  test', 'android'),
(3, 'test', 'this is  test', 'android'),
(4, 'hhdjwj', 'bdjjsksbsbus', 'android'),
(5, 'no me gusta la app', 'va', 'android'),
(6, 'Tony', 'Ton', 'iOS'),
(7, 'Tony', 'Ton', 'iOS'),
(8, 'Tony', 'Ton', 'iOS'),
(9, 'r', 'Rr', 'iOS');

-- --------------------------------------------------------

--
-- Table structure for table `socialaccount_socialaccount`
--

CREATE TABLE `socialaccount_socialaccount` (
  `id` int(11) NOT NULL,
  `provider` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime(6) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `extra_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `socialaccount_socialaccount`
--

INSERT INTO `socialaccount_socialaccount` (`id`, `provider`, `uid`, `last_login`, `date_joined`, `extra_data`, `user_id`) VALUES
(1, 'google', '109432396741172802889', '2019-11-15 22:47:14.302149', '2019-10-10 23:10:46.390615', '{\"family_name\": \"Gleason M\\u00e9ndez\", \"name\": \"Alejandro Gleason M\\u00e9ndez\", \"picture\": \"https://lh3.googleusercontent.com/a-/AAuE7mD5hl5S78KKmp-M7UR1z0uzKNpnlXu4eiJTXpR1\", \"locale\": \"es\", \"given_name\": \"Alejandro\", \"id\": \"109432396741172802889\"}', 6),
(2, 'facebook', '3633594343332906', '2019-11-03 03:22:46.629701', '2019-10-10 23:11:56.528537', '{\"first_name\": \"Alejandro\", \"last_name\": \"Gleason\", \"id\": \"3633594343332906\", \"name\": \"Alejandro Gleason\"}', 7),
(3, 'google', '109935185749118821911', '2019-10-25 19:07:18.604765', '2019-10-25 19:07:18.604846', '{\"family_name\": \"M\", \"name\": \"Gleezy M\", \"picture\": \"https://lh3.googleusercontent.com/-0IYi1Aq2dmQ/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rcJ0DlgS43TBs-P8K8Z1asP-x4ZLg/photo.jpg\", \"locale\": \"es-419\", \"given_name\": \"Gleezy\", \"id\": \"109935185749118821911\"}', 17);

-- --------------------------------------------------------

--
-- Table structure for table `socialaccount_socialapp`
--

CREATE TABLE `socialaccount_socialapp` (
  `id` int(11) NOT NULL,
  `provider` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `socialaccount_socialapp`
--

INSERT INTO `socialaccount_socialapp` (`id`, `provider`, `name`, `client_id`, `secret`, `key`) VALUES
(1, 'google', 'Google Login', '653527979472-4f67umd1kieeale5tue67692u9h8imi7.apps.googleusercontent.com', '7jZZX9rjnVWSGLy5soLByEpl', ''),
(2, 'facebook', 'Fb Login', '498243121000032', 'cf23592d41ff7b7ae8713ab53471bea9', '');

-- --------------------------------------------------------

--
-- Table structure for table `socialaccount_socialapp_sites`
--

CREATE TABLE `socialaccount_socialapp_sites` (
  `id` int(11) NOT NULL,
  `socialapp_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `socialaccount_socialapp_sites`
--

INSERT INTO `socialaccount_socialapp_sites` (`id`, `socialapp_id`, `site_id`) VALUES
(1, 1, 2),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `socialaccount_socialtoken`
--

CREATE TABLE `socialaccount_socialtoken` (
  `id` int(11) NOT NULL,
  `token` longtext COLLATE utf8_unicode_ci NOT NULL,
  `token_secret` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expires_at` datetime(6) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `socialaccount_socialtoken`
--

INSERT INTO `socialaccount_socialtoken` (`id`, `token`, `token_secret`, `expires_at`, `account_id`, `app_id`) VALUES
(1, 'ya29.Il-xBx9Q27c4sf30zrm1Cd1CpXUWa3hFY0LxQTqQiDJAZbZJWF_CTOwghcVPR_0LwXDD4uqK70WpVmmGoelicsiFyAJgZUpRRfNYkyiIFKo0PhoSCRfwwDFcIL_Syvt1Tw', '', '2019-11-15 23:47:13.383758', 1, 1),
(2, 'EAAHFJkQ9LmABAKuXQbRwoQnIJUzs3E1hKGXwk1xjubPqfABMDR8vMgTzh82KuDLZBe3MloS8KW0x1EY2lRRpZBVZBdqnfMoAFBDtP9P9jDMyJvTnmAkDqDDftgSrKQ5ipWPyZAiGQ8ZByRHAaZBNqZAXLBBBFeBHWyk5LFdi1w2BQZDZD', '', '2020-01-02 03:22:25.734991', 2, 2),
(3, 'ya29.Il-pB_M6FbzlCa-svDb0EutlPo0Wfm_leiIurruSEwrxtdHla1iZfRlzjzHBJM2S_ZBWm3a4l-HERLfF6O-EWMutmDEZ5u9jBc9tB7hgUCr0lTb-U9opCw03RIFLsLUD2w', '', '2019-10-25 20:07:14.959405', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_association`
--

CREATE TABLE `social_auth_association` (
  `id` int(11) NOT NULL,
  `server_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issued` int(11) NOT NULL,
  `lifetime` int(11) NOT NULL,
  `assoc_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_code`
--

CREATE TABLE `social_auth_code` (
  `id` int(11) NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `timestamp` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_nonce`
--

CREATE TABLE `social_auth_nonce` (
  `id` int(11) NOT NULL,
  `server_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  `salt` varchar(65) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_partial`
--

CREATE TABLE `social_auth_partial` (
  `id` int(11) NOT NULL,
  `token` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `next_step` smallint(5) UNSIGNED NOT NULL,
  `backend` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_auth_usersocialauth`
--

CREATE TABLE `social_auth_usersocialauth` (
  `id` int(11) NOT NULL,
  `provider` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extra_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_auth_usersocialauth`
--

INSERT INTO `social_auth_usersocialauth` (`id`, `provider`, `uid`, `extra_data`, `user_id`) VALUES
(1, 'twitter', '443947300', '{\"auth_time\": 1572039997, \"access_token\": {\"oauth_token_secret\": \"TDAWBNXFNHPOE9iBH6O0aHs1TJkLoOV0RhtSaEnakvYoR\", \"oauth_token\": \"443947300-uo1neeUGAxFrUyGXLjze2zuFbIhuuGN7UVBm1WvB\", \"user_id\": \"443947300\", \"screen_name\": \"IrvingPercam\"}, \"id\": 443947300}', 2),
(2, 'github', '43660923', '{\"access_token\": \"a6ad9678cd77b2c4a060d4271700b124eb312e82\", \"expires\": null, \"token_type\": \"bearer\", \"auth_time\": 1573756953, \"login\": \"alegleason\", \"id\": 43660923}', 3),
(3, 'twitter', '303429685', '{\"access_token\": {\"oauth_token_secret\": \"Vhwo0NRROXGTTKicYCgGB9UeTFTFxfL0JwKmyYdOIAceK\", \"oauth_token\": \"303429685-YXUy5rM9vIU48mXYrSef2eGnoJ7EdHZGZHgh1TJk\", \"user_id\": \"303429685\", \"screen_name\": \"AlexGleasonM\"}, \"id\": 303429685, \"auth_time\": 1570748470}', 5),
(4, 'linkedin-oauth2', '9qkea0WKlk', '{\"last_name\": {\"preferredLocale\": {\"country\": \"ES\", \"language\": \"es\"}, \"localized\": {\"es_ES\": \"Gleason M\\u00e9ndez\"}}, \"expires\": 5183999, \"token_type\": null, \"email_address\": null, \"id\": \"9qkea0WKlk\", \"first_name\": {\"preferredLocale\": {\"country\": \"ES\", \"language\": \"es\"}, \"localized\": {\"es_ES\": \"Alejandro\"}}, \"name\": null, \"access_token\": \"AQUOz42VkYdZYt4Yecdyx_w_USbmDv8vLMta7XuPpkEg5cq_Lf6FsHmgbXnPQqFxKUkFObD_WS4P7m3GS4GpAxrLbfPmhjz0uvTbbS0LqnR3i8uTO6ieN_1cMxO3DGjyvuH7a_XSLdXqW7H64Vey15u_YcCvYmWoaiYtalyXY1ZNHbgXIRl7-8ckV2eIAuEJMBPIEhMhsRaow2p9KaFOdFvlUYeNdKXHvbHTOfSivSpHxhzF4J1KyX5knXmiuUuJdJailbQaMwQYGfpeH0IiGs4AGgbmySmZ-sSPzpysu1bxKDRIiqaU-ztHnnzQR20DOfyIIYqxFkc1JMi7wO4h8HHyUD6VtA\", \"picture_url\": null, \"auth_time\": 1573687274, \"profile_url\": null}', 10);

-- --------------------------------------------------------

--
-- Table structure for table `users_appoinments`
--

CREATE TABLE `users_appoinments` (
  `idAppointment` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `carnet_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `idService_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_appoinments`
--

INSERT INTO `users_appoinments` (`idAppointment`, `date`, `status`, `carnet_id`, `idService_id`) VALUES
(38, '2019-10-01 06:00:00', 1, 'a', 1),
(39, '2019-10-22 00:00:00', 1, 'hola', 1),
(40, '2020-02-22 17:15:34', 0, 'a', 1),
(44, '2019-10-26 00:00:00', 0, 'a', 1),
(45, '2019-10-07 11:47:18', 0, 'a', 1),
(46, '2019-10-09 11:53:45', 1, 'hola', 1),
(47, '2019-10-07 12:07:12', 0, 'a', 1),
(48, '2019-10-21 12:09:11', 0, 'a', 1),
(49, '2019-10-07 12:13:38', 0, 'a', 1),
(50, '2020-11-11 11:11:11', 0, 'a', 1),
(51, '2020-10-07 12:15:53', 0, 'hola', 1),
(52, '2019-10-09 17:54:17', 1, 'a', 1),
(53, '2026-01-21 17:51:40', 1, 'hola', 1),
(54, '2019-10-09 18:19:20', 0, 'a', 1),
(55, '2019-08-20 21:27:03', 0, 'a', 1),
(56, '2019-12-07 23:53:06', 0, 'a', 1),
(57, '2019-09-05 11:28:23', 1, 'a', 1),
(58, '2019-10-06 11:37:12', 1, 'hola', 1),
(59, '2019-10-12 16:27:01', 0, 'a', 1),
(60, '2019-10-03 16:27:01', 1, 'hola', 1),
(61, '2019-10-10 17:43:21', 0, 'a', 1),
(62, '2019-10-17 00:23:55', 1, 'hola', 1),
(63, '2019-10-13 09:27:22', 1, 'hola', 1),
(64, '2019-10-10 10:53:27', 0, 'a', 1),
(65, '2019-10-19 10:52:49', 0, 'a', 1),
(66, '2019-10-12 00:12:43', 0, 'a', 1),
(67, '2019-10-10 11:11:48', 0, 'a', 1),
(68, '2019-08-19 10:52:49', 1, 'a', 1),
(69, '2019-08-20 10:52:49', 1, 'a', 1),
(70, '2019-08-21 10:52:49', 1, 'a', 1),
(71, '2019-10-15 11:35:32', 0, 'a', 1),
(72, '2019-10-12 11:35:55', 0, 'a', 1),
(73, '2019-10-09 15:47:28', 1, 'a', 1),
(74, '2019-10-21 15:03:02', 0, 'a', 1),
(75, '2019-10-09 19:00:38', 1, 'a', 1),
(76, '2019-10-09 23:00:05', 1, 'a', 1),
(77, '2019-10-09 16:09:28', 1, 'a', 1),
(78, '2019-10-10 12:19:42', 1, 'a', 1),
(79, '2019-10-11 12:23:19', 1, 'a', 1),
(80, '2019-10-09 21:26:10', 1, 'a', 1),
(81, '2019-10-10 18:30:20', 1, 'a', 1),
(82, '2019-10-23 18:42:45', 1, 'a', 1),
(83, '2019-11-15 18:42:45', 0, 'a', 1),
(84, '2020-11-11 11:11:11', 0, 'a', 1),
(86, '2019-10-15 12:59:22', 1, 'a', 2),
(87, '2019-10-29 13:01:31', 0, 'a', 4),
(88, '2019-10-14 13:00:00', 1, 'a', 3),
(89, '2019-10-14 13:00:00', 1, 'hola', 2),
(90, '2019-10-14 16:00:00', 1, 'hola', 3),
(91, '2021-10-19 17:12:00', 0, 'hola', 4),
(92, '2019-10-14 12:00:00', 1, 'hola', 3),
(93, '2019-10-14 17:22:00', 1, 'hola', 3),
(94, '2023-10-16 17:22:00', 0, 'hola', 3),
(400, '2019-10-01 06:00:00', 1, 'a', 8),
(401, '2025-10-22 18:16:00', 0, 'hola', 4),
(402, '2019-12-12 16:03:00', 1, 'hola', 4),
(403, '2019-10-21 08:00:00', 1, 'hola', 1),
(404, '2019-10-19 10:00:00', 1, 'hola', 4),
(405, '2019-10-19 10:00:00', 1, 'hola', 4),
(406, '2019-10-19 10:00:00', 1, 'hola', 4),
(407, '2019-10-19 10:00:00', 1, 'hola', 4),
(408, '2019-10-19 10:00:00', 1, 'hola', 4),
(409, '2019-10-19 10:00:00', 1, 'hola', 4),
(410, '2019-10-19 10:00:00', 1, 'hola', 4),
(411, '2019-10-19 10:00:00', 1, 'hola', 4),
(412, '2019-10-19 10:00:00', 1, 'hola', 4),
(413, '2019-10-19 10:00:00', 1, 'hola', 4),
(414, '2019-10-19 10:00:00', 1, 'hola', 4),
(415, '2019-10-19 10:00:00', 1, 'hola', 4),
(416, '2019-10-19 10:00:00', 1, 'hola', 4),
(417, '2019-10-19 10:00:00', 1, 'hola', 4),
(418, '2019-10-19 10:00:00', 1, 'hola', 4),
(419, '2019-10-19 10:00:00', 1, 'hola', 4),
(420, '2019-10-19 21:21:33', 1, 'a', 8),
(421, '2019-10-19 10:00:00', 1, 'hola', 6),
(422, '2019-10-19 10:00:00', 1, 'hola', 6),
(423, '2019-10-29 19:35:24', 0, 'a', 2),
(424, '2019-10-19 19:45:35', 1, 'a', 1),
(425, '2019-10-19 10:00:00', 1, 'hola', 6),
(426, '2019-10-19 10:00:00', 1, 'hola', 6),
(427, '2019-10-19 10:00:00', 1, 'hola', 6),
(428, '2019-10-19 19:48:37', 1, 'a', 3),
(429, '2019-10-19 19:51:48', 1, 'a', 3),
(430, '2019-10-19 10:00:00', 1, 'hola', 4),
(431, '2019-10-19 10:00:00', 1, 'hola', 4),
(432, '2019-10-19 19:54:21', 1, 'a', 1),
(433, '2019-10-21 12:00:00', 1, 'hola', 4),
(434, '2019-10-19 11:00:00', 1, 'hola', 4),
(435, '2019-10-21 10:00:00', 1, 'hola', 2),
(436, '2019-10-21 10:00:00', 1, 'hola', 2),
(437, '2019-10-21 09:00:00', 1, 'hola', 2),
(438, '2019-10-21 09:00:00', 1, 'hola', 2),
(439, '2019-10-23 13:00:00', 1, 'hola', 4),
(440, '2019-10-23 13:00:00', 1, 'hola', 4),
(441, '2019-10-23 13:00:00', 1, 'hola', 4),
(442, '2019-10-23 13:00:00', 0, 'hola', 4),
(443, '2019-10-23 13:00:00', 0, 'hola', 4),
(444, '2019-10-21 08:00:00', 1, 'hola', 2),
(445, '2019-10-22 13:00:00', 0, 'hola', 4),
(446, '2019-10-25 12:00:00', 0, 'hola', 4),
(447, '2019-10-22 13:00:00', 1, 'hola', 4),
(448, '2019-10-21 11:00:00', 1, 'hola', 7),
(449, '2019-10-22 12:00:00', 1, 'hola', 5),
(450, '2019-10-21 13:00:00', 1, 'hola', 3),
(451, '2019-10-24 11:00:00', 1, 'hola', 4),
(452, '2019-10-25 13:00:00', 1, 'hola', 7),
(453, '2019-11-01 21:48:09', 0, 'a', 1),
(454, '2019-10-27 22:11:50', 0, 'a', 1),
(455, '2019-10-24 13:00:00', 1, 'hola', 4),
(456, '2019-10-24 14:00:00', 1, 'hola', 4),
(457, '2019-10-24 08:00:00', 1, 'hola', 6),
(458, '2019-10-24 12:00:00', 1, 'hola', 4),
(459, '2019-10-24 12:00:00', 1, 'hola', 6),
(460, '2019-10-24 10:00:00', 1, 'hola', 6),
(461, '2019-10-24 15:00:00', 1, 'hola', 6),
(462, '2019-10-24 12:00:00', 1, 'hola', 8),
(463, '2019-10-24 11:00:00', 1, 'hola', 7),
(464, '2019-10-24 12:00:00', 1, 'hola', 3),
(465, '2019-10-24 14:00:00', 1, 'hola', 4),
(466, '2019-10-24 10:00:00', 1, 'hola', 3),
(467, '2019-10-24 10:00:00', 1, 'hola', 6),
(468, '2019-10-24 10:00:00', 1, 'hola', 7),
(469, '2019-10-24 08:00:00', 1, 'hola', 8),
(470, '2019-10-24 08:00:00', 1, 'hola', 8),
(471, '2019-10-24 13:00:00', 1, 'hola', 5),
(472, '2019-10-24 09:00:00', 1, 'hola', 3),
(473, '2019-10-24 13:00:00', 1, 'hola', 4),
(474, '2019-10-24 10:00:00', 1, 'hola', 5),
(475, '2019-10-24 13:00:00', 1, 'hola', 5),
(476, '2019-10-24 13:00:00', 1, 'hola', 7),
(477, '2019-10-25 14:00:00', 1, 'hola', 2),
(478, '2019-10-25 13:00:00', 1, 'hola', 3),
(479, '2019-10-28 12:00:00', 1, 'hola', 5),
(480, '2019-10-31 14:00:00', 1, 'hola', 8),
(481, '2019-10-31 13:00:00', 1, 'hola', 4),
(482, '2021-03-25 12:00:00', 1, 'hola', 3),
(483, '2019-10-31 16:40:38', 1, 'a', 3),
(484, '2019-10-28 21:16:02', 1, 'a', 5),
(485, '2019-10-31 15:00:00', 1, 'hola', 8),
(486, '2019-10-26 12:00:00', 1, 'hola', 8),
(487, '2019-10-30 14:00:00', 1, 'hola', 5),
(488, '2019-10-31 13:00:00', 1, 'hola', 3),
(489, '2019-10-31 15:01:44', 0, 'a', 6),
(490, '2019-10-29 15:00:48', 0, 'a', 3),
(491, '2019-10-29 16:40:18', 1, 'a', 5),
(492, '2019-10-29 11:00:00', 1, 'hola', 2),
(493, '2020-11-11 11:11:11', 1, 'a', 1),
(494, '2020-11-11 11:11:11', 1, 'a', 1),
(495, '2020-11-11 11:11:11', 1, 'a', 1),
(496, '2019-11-29 21:54:19', 1, 'a', 4),
(497, '2019-11-04 18:00:52', 1, 'a', 6),
(498, '2019-11-28 18:07:06', 1, 'a', 8),
(499, '2019-11-02 18:09:41', 1, 'a', 5),
(500, '2020-11-11 11:12:54', 1, 'a', 1),
(501, '2019-10-27 19:15:38', 1, 'a', 2),
(502, '2019-10-27 19:15:58', 1, 'a', 2),
(503, '2019-10-27 19:15:09', 1, 'a', 2),
(504, '2019-10-27 19:15:23', 1, 'a', 2),
(505, '2019-10-27 19:15:33', 1, 'a', 2),
(506, '2020-11-11 12:25:11', 1, 'a', 1),
(507, '2020-11-11 00:25:50', 1, 'a', 1),
(508, '2020-11-11 12:25:11', 1, 'a', 1),
(509, '2020-11-11 12:25:11', 1, 'a', 1),
(510, '2019-10-31 18:58:34', 1, 'a', 1),
(511, '2019-10-27 20:00:54', 1, 'a', 1),
(512, '2019-10-27 20:01:09', 1, 'a', 1),
(513, '2019-10-27 20:03:21', 1, 'a', 1),
(514, '2019-10-27 20:06:35', 1, 'a', 1),
(515, '2019-10-27 20:09:49', 1, 'a', 1),
(516, '2019-10-27 20:09:49', 1, 'a', 1),
(517, '2019-10-27 20:09:49', 1, 'a', 1),
(518, '2019-10-27 22:14:03', 1, 'a', 1),
(519, '2019-10-27 09:14:12', 1, 'a', 1),
(520, '2019-10-27 18:14:19', 1, 'a', 1),
(521, '2019-10-27 13:14:27', 1, 'a', 1),
(522, '2019-12-02 17:21:11', 1, 'a', 4),
(523, '2019-11-11 17:23:57', 1, 'a', 2),
(524, '2019-11-30 17:31:17', 1, 'a', 2),
(525, '2019-11-30 17:31:17', 1, 'a', 5),
(526, '2019-11-11 17:31:35', 1, 'a', 6),
(527, '2019-11-05 17:51:58', 1, 'a', 2),
(528, '2019-11-16 00:00:00', 1, 'hola', 2),
(529, '2019-11-05 13:00:00', 1, 'hola', 2),
(530, '2019-11-05 14:00:00', 1, 'hola', 2),
(531, '2019-11-07 12:00:00', 1, 'hola', 2),
(532, '2019-11-07 15:00:00', 1, 'hola', 2),
(533, '2019-11-07 13:00:00', 1, 'hola', 2),
(534, '2019-11-08 13:00:00', 1, 'hola', 2),
(535, '2019-11-14 14:00:00', 1, 'hola', 2),
(536, '2019-11-08 16:00:00', 1, 'hola', 2),
(537, '2019-11-08 13:00:00', 1, 'hola', 2),
(538, '2019-11-07 13:00:00', 1, 'hola', 2),
(539, '2019-11-09 14:00:00', 1, 'hola', 2),
(540, '2019-11-09 13:00:00', 1, 'hola', 2),
(541, '2019-11-14 12:00:00', 1, 'hola', 2),
(542, '2019-11-27 14:00:00', 1, 'hola', 2),
(543, '2019-11-20 15:00:00', 1, 'hola', 2),
(544, '2019-11-08 15:00:00', 1, 'hola', 2),
(545, '2019-11-21 14:00:00', 1, 'hola', 2),
(546, '2019-11-08 14:00:00', 1, 'hola', 2),
(547, '2019-11-15 13:00:00', 1, 'hola', 2),
(548, '2019-11-16 14:00:00', 1, 'hola', 2),
(549, '2019-11-23 12:00:00', 1, 'hola', 2),
(550, '2019-11-28 15:00:00', 1, 'hola', 2),
(551, '2019-11-27 15:00:00', 1, 'hola', 2),
(552, '2019-11-26 14:00:00', 1, 'hola', 2),
(553, '2019-11-21 12:00:00', 1, 'hola', 2),
(554, '2019-11-29 13:00:00', 1, 'hola', 2),
(555, '2019-11-25 14:00:00', 1, 'hola', 2),
(556, '2019-11-28 13:00:00', 1, 'hola', 2),
(557, '2019-11-20 14:00:00', 1, 'hola', 2),
(558, '2019-11-15 14:00:00', 1, 'hola', 2),
(559, '2019-11-27 14:00:00', 1, 'hola', 2),
(560, '2019-11-17 14:00:00', 1, 'hola', 2),
(561, '2019-11-19 14:00:00', 1, 'hola', 2),
(562, '2019-12-11 14:00:00', 1, 'hola', 2),
(563, '2019-11-10 14:00:00', 1, 'hola', 2),
(564, '2019-11-13 14:00:00', 1, 'hola', 2),
(565, '2019-11-15 14:00:00', 1, 'hola', 2),
(566, '2019-11-16 14:00:00', 1, 'hola', 2),
(567, '2019-11-22 14:00:00', 1, 'hola', 2),
(568, '2019-11-07 14:00:00', 1, 'hola', 4),
(569, '2019-11-14 14:00:00', 1, 'hola', 4),
(570, '2019-11-19 14:00:00', 1, 'hola', 4),
(571, '2019-11-24 14:00:00', 1, 'hola', 2),
(572, '2019-11-15 14:00:00', 1, 'hola', 2),
(573, '2019-11-09 14:00:00', 1, 'hola', 3),
(574, '2019-11-23 14:00:00', 1, 'hola', 3),
(575, '2019-11-06 14:00:00', 1, 'hola', 2),
(576, '2019-11-09 14:00:00', 1, 'hola', 2),
(577, '2019-11-10 14:00:00', 1, 'hola', 2),
(578, '2019-11-29 14:00:00', 1, 'hola', 2),
(579, '2019-11-12 14:00:00', 1, 'hola', 2),
(580, '2019-11-16 14:00:00', 1, 'hola', 2),
(581, '2019-11-06 14:00:00', 1, 'hola', 2),
(582, '2019-11-06 14:00:00', 1, 'hola', 2),
(583, '2019-11-06 14:00:00', 1, 'hola', 2),
(584, '2019-11-06 14:00:00', 1, 'hola', 2),
(585, '2019-11-06 10:00:00', 1, 'hola', 1),
(586, '2019-11-06 10:00:00', 1, 'hola', 1),
(587, '2019-11-06 10:00:00', 1, 'hola', 1),
(588, '2019-11-06 10:00:00', 1, 'hola', 1),
(589, '2019-11-06 09:00:00', 1, 'hola', 1),
(590, '2019-11-06 09:00:00', 1, 'hola', 1),
(591, '2019-11-06 09:00:00', 1, 'hola', 1),
(592, '2019-11-06 09:00:00', 1, 'hola', 1),
(593, '2019-11-18 13:00:00', 1, 'hola', 2),
(594, '2019-11-07 10:00:00', 1, 'hola', 1),
(595, '2019-11-07 10:00:00', 1, 'hola', 1),
(596, '2019-11-07 10:00:00', 1, 'hola', 1),
(597, '2019-11-07 10:00:00', 1, 'hola', 1),
(598, '2019-11-08 10:00:00', 1, 'hola', 1),
(599, '2019-11-08 10:00:00', 1, 'hola', 1),
(600, '2019-11-08 10:00:00', 1, 'hola', 1),
(601, '2019-11-08 10:00:00', 1, 'hola', 1),
(602, '2019-11-07 14:00:00', 1, 'hola', 1),
(603, '2019-11-06 15:00:00', 1, 'hola', 1),
(604, '2019-11-07 15:00:00', 1, 'hola', 1),
(605, '2019-11-12 14:00:00', 1, 'irvcarnet', 2),
(606, '2019-11-12 09:00:00', 1, 'irvcarnet', 1),
(607, '2019-11-12 09:00:00', 1, 'irvcarnet', 1),
(608, '2019-11-12 09:00:00', 1, 'irvcarnet', 1),
(609, '2019-11-12 09:00:00', 1, 'irvcarnet', 1),
(610, '2019-11-13 09:00:00', 1, 'irvcarnet', 1),
(611, '2019-11-14 10:00:00', 1, 'irvcarnet', 1),
(612, '2019-11-12 08:00:00', 1, 'irvcarnet', 1),
(613, '2019-11-12 12:00:00', 1, 'irvcarnet', 2),
(614, '2099-10-10 00:00:59', 1, 'a', 1),
(615, '2019-11-14 12:00:00', 1, 'irvcarnet', 1),
(616, '2019-11-14 11:00:00', 1, 'irvcarnet', 1),
(617, '2019-11-14 13:00:00', 1, 'irvcarnet', 1),
(618, '2019-11-14 13:00:00', 1, 'irvcarnet', 1),
(619, '2019-11-14 13:00:00', 1, 'irvcarnet', 1),
(620, '2019-11-15 12:00:00', 1, 'irvcarnet', 1),
(621, '2019-11-15 13:00:00', 1, 'irvcarnet', 1),
(622, '2019-11-15 14:00:00', 1, 'irvcarnet', 1),
(623, '2019-11-15 11:00:00', 1, 'irvcarnet', 1),
(624, '2019-11-15 14:00:00', 1, 'irvcarnet', 1),
(625, '2019-11-15 14:00:00', 1, 'irvcarnet', 1),
(626, '2019-11-19 12:00:00', 1, 'irvcarnet', 1),
(627, '2019-11-27 13:00:00', 1, 'irvcarnet', 1),
(628, '2019-11-21 13:00:00', 1, 'irvcarnet', 1),
(629, '2019-11-18 14:00:00', 1, 'irvcarnet', 1),
(630, '2019-11-26 13:00:00', 1, 'irvcarnet', 1),
(631, '2019-11-28 13:00:00', 1, 'irvcarnet', 1),
(632, '2019-11-28 11:46:00', 1, 'irvcarnet', 1),
(633, '2019-11-29 14:00:00', 1, 'irvcarnet', 1),
(634, '2019-11-14 15:00:00', 1, 'irvcarnet', 1),
(635, '2019-11-22 14:00:00', 1, 'irvcarnet', 1),
(636, '2019-11-29 10:00:00', 1, 'irvcarnet', 1),
(637, '2019-11-28 11:00:00', 1, 'irvcarnet', 1),
(638, '2019-11-28 13:00:00', 1, 'irvcarnet', 1),
(639, '2019-11-25 11:00:00', 1, 'irvcarnet', 1),
(640, '2019-11-26 11:00:00', 1, 'irvcarnet', 1),
(641, '2019-11-27 12:00:00', 1, 'irvcarnet', 1),
(642, '2019-11-27 11:00:00', 1, 'irvcarnet', 1),
(643, '2019-11-29 11:00:00', 1, 'irvcarnet', 1),
(644, '2019-11-28 10:00:00', 1, 'irvcarnet', 1),
(645, '2019-11-25 10:00:00', 1, 'irvcarnet', 1),
(646, '2019-11-28 12:00:00', 1, 'irvcarnet', 2),
(647, '2019-11-28 08:00:00', 1, 'irvcarnet', 1),
(648, '2019-11-26 08:00:00', 1, 'irvcarnet', 1),
(649, '2019-11-30 15:00:00', 1, 'irvcarnet', 2),
(650, '2019-11-25 08:00:00', 1, 'irvcarnet', 1),
(651, '2019-11-23 12:00:00', 1, 'irvcarnet', 2),
(652, '2019-11-28 12:00:00', 1, 'irvcarnet', 2),
(653, '2019-11-26 12:00:00', 1, 'irvcarnet', 2),
(654, '2019-11-26 12:00:00', 1, 'irvcarnet', 2),
(655, '2019-11-27 08:00:00', 1, 'irvcarnet', 1),
(656, '2019-11-21 12:00:00', 1, 'irvcarnet', 2),
(657, '2019-11-22 12:00:00', 1, 'irvcarnet', 2),
(658, '2019-11-20 12:00:00', 1, 'irvcarnet', 2),
(659, '2019-11-30 12:00:00', 1, 'irvcarnet', 2),
(660, '2019-11-26 12:00:00', 1, 'irvcarnet', 2),
(661, '2019-11-24 12:00:00', 1, 'irvcarnet', 2),
(662, '2019-11-18 12:00:00', 1, 'irvcarnet', 2),
(663, '2019-11-17 12:00:00', 1, 'irvcarnet', 2),
(664, '2019-12-12 00:00:00', 1, 'a', 1),
(665, '2020-12-12 00:00:00', 1, 'a', 1),
(666, '2020-12-12 00:00:00', 1, 'a', 1),
(667, '2020-12-12 00:00:00', 1, 'a', 1),
(668, '2020-10-10 00:00:00', 1, 'a', 1),
(669, '2020-10-10 00:00:00', 1, 'a', 1),
(670, '2021-01-12 17:01:34', 1, 'a', 8),
(671, '2020-10-10 00:00:00', 1, 'a', 1),
(672, '2020-10-10 00:00:00', 1, 'a', 1),
(673, '2020-10-10 00:00:00', 1, 'a', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_carnetmodel`
--

CREATE TABLE `users_carnetmodel` (
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `idCarnet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_carnetmodel`
--

INSERT INTO `users_carnetmodel` (`name`, `status`, `idCarnet`) VALUES
('a', 1, 1),
('A74S22F', 1, 10),
('cosmetics', 1, 6),
('hola', 1, 2),
('irvcarnet', 1, 9),
('jenner', 1, 5),
('kylie', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users_service`
--

CREATE TABLE `users_service` (
  `idService` int(11) NOT NULL,
  `nameService` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `startTime` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `endTime` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `startDay` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `endDay` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `numOffices` int(11) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_service`
--

INSERT INTO `users_service` (`idService`, `nameService`, `startTime`, `endTime`, `startDay`, `endDay`, `numOffices`, `duration`, `status`) VALUES
(1, 'Cataratas', '8:00:00', '16:00:00', 'L', 'V', 4, 1, 1),
(2, 'INAPAM', '12:00:00', '18:00:00', 'L', 'D', 10, 1, 1),
(3, 'Prótesis ocular', '10:00:00', '16:00:00', 'S', 'S', 1, 2, 1),
(4, 'Baja visión', '13:00:00', '18:00:00', 'L', 'S', 5, 1, 1),
(5, 'Imagenología', '10:00:00', '13:00:00', 'MI', 'MI', 2, 2, 1),
(6, 'Retina', '7:00:00', '13:00:00', 'L', 'S', 3, 1, 1),
(7, 'Segmento anterior & córnea', '13:00:00', '16:00:00', 'S', 'S', 1, 1, 1),
(8, 'Glaucoma', '8:00:00', '16:00:00', 'MI', 'V', 2, 3, 1),
(9, 'Oftalmopediatria', '7:00:00', '13:00:00', 'L', 'J', 1, 1, 1),
(11, 'Consulta básica', '12:00:00', '20:00:00', 'L', 'V', 5, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_emailaddress`
--
ALTER TABLE `account_emailaddress`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `account_emailaddress_user_id_2c513194_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `account_emailconfirmation`
--
ALTER TABLE `account_emailconfirmation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`),
  ADD KEY `account_emailconfirm_email_address_id_5b7f8c58_fk_account_e` (`email_address_id`);

--
-- Indexes for table `Appointment`
--
ALTER TABLE `Appointment`
  ADD PRIMARY KEY (`idAppointment`),
  ADD KEY `foreign key` (`carnet`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `celery_taskmeta`
--
ALTER TABLE `celery_taskmeta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_id` (`task_id`),
  ADD KEY `celery_taskmeta_hidden_23fd02dc` (`hidden`);

--
-- Indexes for table `celery_tasksetmeta`
--
ALTER TABLE `celery_tasksetmeta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `taskset_id` (`taskset_id`),
  ADD KEY `celery_tasksetmeta_hidden_593cfc24` (`hidden`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `django_site`
--
ALTER TABLE `django_site`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`);

--
-- Indexes for table `djcelery_crontabschedule`
--
ALTER TABLE `djcelery_crontabschedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `djcelery_intervalschedule`
--
ALTER TABLE `djcelery_intervalschedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `djcelery_periodictask`
--
ALTER TABLE `djcelery_periodictask`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` (`crontab_id`),
  ADD KEY `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` (`interval_id`);

--
-- Indexes for table `djcelery_periodictasks`
--
ALTER TABLE `djcelery_periodictasks`
  ADD PRIMARY KEY (`ident`);

--
-- Indexes for table `djcelery_taskstate`
--
ALTER TABLE `djcelery_taskstate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_id` (`task_id`),
  ADD KEY `djcelery_taskstate_state_53543be4` (`state`),
  ADD KEY `djcelery_taskstate_name_8af9eded` (`name`),
  ADD KEY `djcelery_taskstate_tstamp_4c3f93a1` (`tstamp`),
  ADD KEY `djcelery_taskstate_hidden_c3905e57` (`hidden`),
  ADD KEY `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` (`worker_id`);

--
-- Indexes for table `djcelery_workerstate`
--
ALTER TABLE `djcelery_workerstate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hostname` (`hostname`),
  ADD KEY `djcelery_workerstate_last_heartbeat_4539b544` (`last_heartbeat`);

--
-- Indexes for table `djkombu_message`
--
ALTER TABLE `djkombu_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `djkombu_message_visible_3ca5f33e` (`visible`),
  ADD KEY `djkombu_message_sent_at_680ecd55` (`sent_at`),
  ADD KEY `djkombu_message_queue_id_38d205a7_fk_djkombu_queue_id` (`queue_id`);

--
-- Indexes for table `djkombu_queue`
--
ALTER TABLE `djkombu_queue`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socialaccount_socialaccount`
--
ALTER TABLE `socialaccount_socialaccount`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `socialaccount_socialaccount_provider_uid_fc810c6e_uniq` (`provider`,`uid`),
  ADD KEY `socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `socialaccount_socialapp`
--
ALTER TABLE `socialaccount_socialapp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socialaccount_socialapp_sites`
--
ALTER TABLE `socialaccount_socialapp_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `socialaccount_socialapp_sites_socialapp_id_site_id_71a9a768_uniq` (`socialapp_id`,`site_id`),
  ADD KEY `socialaccount_socialapp_sites_site_id_2579dee5_fk_django_site_id` (`site_id`);

--
-- Indexes for table `socialaccount_socialtoken`
--
ALTER TABLE `socialaccount_socialtoken`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq` (`app_id`,`account_id`),
  ADD KEY `socialaccount_social_account_id_951f210e_fk_socialacc` (`account_id`);

--
-- Indexes for table `social_auth_association`
--
ALTER TABLE `social_auth_association`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_auth_association_server_url_handle_078befa2_uniq` (`server_url`,`handle`);

--
-- Indexes for table `social_auth_code`
--
ALTER TABLE `social_auth_code`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_auth_code_email_code_801b2d02_uniq` (`email`,`code`),
  ADD KEY `social_auth_code_code_a2393167` (`code`),
  ADD KEY `social_auth_code_timestamp_176b341f` (`timestamp`);

--
-- Indexes for table `social_auth_nonce`
--
ALTER TABLE `social_auth_nonce`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_auth_nonce_server_url_timestamp_salt_f6284463_uniq` (`server_url`,`timestamp`,`salt`);

--
-- Indexes for table `social_auth_partial`
--
ALTER TABLE `social_auth_partial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_auth_partial_token_3017fea3` (`token`),
  ADD KEY `social_auth_partial_timestamp_50f2119f` (`timestamp`);

--
-- Indexes for table `social_auth_usersocialauth`
--
ALTER TABLE `social_auth_usersocialauth`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `social_auth_usersocialauth_provider_uid_e6b5e668_uniq` (`provider`,`uid`),
  ADD KEY `social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `users_appoinments`
--
ALTER TABLE `users_appoinments`
  ADD PRIMARY KEY (`idAppointment`),
  ADD KEY `users_appoinments_carnet_id_20aad664_fk_users_carnetmodel_name` (`carnet_id`),
  ADD KEY `users_appoinments_idService_id_948d52d0_fk_users_ser` (`idService_id`);

--
-- Indexes for table `users_carnetmodel`
--
ALTER TABLE `users_carnetmodel`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `id` (`idCarnet`),
  ADD UNIQUE KEY `users_carnetmodel_idCarnet_cad0f3bb_uniq` (`idCarnet`);

--
-- Indexes for table `users_service`
--
ALTER TABLE `users_service`
  ADD PRIMARY KEY (`idService`),
  ADD UNIQUE KEY `nameService` (`nameService`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_emailaddress`
--
ALTER TABLE `account_emailaddress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `account_emailconfirmation`
--
ALTER TABLE `account_emailconfirmation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Appointment`
--
ALTER TABLE `Appointment`
  MODIFY `idAppointment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `celery_taskmeta`
--
ALTER TABLE `celery_taskmeta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `celery_tasksetmeta`
--
ALTER TABLE `celery_tasksetmeta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `django_site`
--
ALTER TABLE `django_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `djcelery_crontabschedule`
--
ALTER TABLE `djcelery_crontabschedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `djcelery_intervalschedule`
--
ALTER TABLE `djcelery_intervalschedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `djcelery_periodictask`
--
ALTER TABLE `djcelery_periodictask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `djcelery_taskstate`
--
ALTER TABLE `djcelery_taskstate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `djcelery_workerstate`
--
ALTER TABLE `djcelery_workerstate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `djkombu_message`
--
ALTER TABLE `djkombu_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `djkombu_queue`
--
ALTER TABLE `djkombu_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `socialaccount_socialaccount`
--
ALTER TABLE `socialaccount_socialaccount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `socialaccount_socialapp`
--
ALTER TABLE `socialaccount_socialapp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `socialaccount_socialapp_sites`
--
ALTER TABLE `socialaccount_socialapp_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `socialaccount_socialtoken`
--
ALTER TABLE `socialaccount_socialtoken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `social_auth_association`
--
ALTER TABLE `social_auth_association`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_auth_code`
--
ALTER TABLE `social_auth_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_auth_nonce`
--
ALTER TABLE `social_auth_nonce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_auth_partial`
--
ALTER TABLE `social_auth_partial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_auth_usersocialauth`
--
ALTER TABLE `social_auth_usersocialauth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_appoinments`
--
ALTER TABLE `users_appoinments`
  MODIFY `idAppointment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=674;

--
-- AUTO_INCREMENT for table `users_carnetmodel`
--
ALTER TABLE `users_carnetmodel`
  MODIFY `idCarnet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users_service`
--
ALTER TABLE `users_service`
  MODIFY `idService` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_emailaddress`
--
ALTER TABLE `account_emailaddress`
  ADD CONSTRAINT `account_emailaddress_user_id_2c513194_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `account_emailconfirmation`
--
ALTER TABLE `account_emailconfirmation`
  ADD CONSTRAINT `account_emailconfirm_email_address_id_5b7f8c58_fk_account_e` FOREIGN KEY (`email_address_id`) REFERENCES `account_emailaddress` (`id`);

--
-- Constraints for table `Appointment`
--
ALTER TABLE `Appointment`
  ADD CONSTRAINT `foreign key` FOREIGN KEY (`carnet`) REFERENCES `users_carnetmodel` (`name`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `djcelery_periodictask`
--
ALTER TABLE `djcelery_periodictask`
  ADD CONSTRAINT `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` FOREIGN KEY (`crontab_id`) REFERENCES `djcelery_crontabschedule` (`id`),
  ADD CONSTRAINT `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` FOREIGN KEY (`interval_id`) REFERENCES `djcelery_intervalschedule` (`id`);

--
-- Constraints for table `djcelery_taskstate`
--
ALTER TABLE `djcelery_taskstate`
  ADD CONSTRAINT `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` FOREIGN KEY (`worker_id`) REFERENCES `djcelery_workerstate` (`id`);

--
-- Constraints for table `djkombu_message`
--
ALTER TABLE `djkombu_message`
  ADD CONSTRAINT `djkombu_message_queue_id_38d205a7_fk_djkombu_queue_id` FOREIGN KEY (`queue_id`) REFERENCES `djkombu_queue` (`id`);

--
-- Constraints for table `socialaccount_socialaccount`
--
ALTER TABLE `socialaccount_socialaccount`
  ADD CONSTRAINT `socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `socialaccount_socialapp_sites`
--
ALTER TABLE `socialaccount_socialapp_sites`
  ADD CONSTRAINT `socialaccount_social_socialapp_id_97fb6e7d_fk_socialacc` FOREIGN KEY (`socialapp_id`) REFERENCES `socialaccount_socialapp` (`id`),
  ADD CONSTRAINT `socialaccount_socialapp_sites_site_id_2579dee5_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`);

--
-- Constraints for table `socialaccount_socialtoken`
--
ALTER TABLE `socialaccount_socialtoken`
  ADD CONSTRAINT `socialaccount_social_account_id_951f210e_fk_socialacc` FOREIGN KEY (`account_id`) REFERENCES `socialaccount_socialaccount` (`id`),
  ADD CONSTRAINT `socialaccount_social_app_id_636a42d7_fk_socialacc` FOREIGN KEY (`app_id`) REFERENCES `socialaccount_socialapp` (`id`);

--
-- Constraints for table `social_auth_usersocialauth`
--
ALTER TABLE `social_auth_usersocialauth`
  ADD CONSTRAINT `social_auth_usersocialauth_user_id_17d28448_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `users_appoinments`
--
ALTER TABLE `users_appoinments`
  ADD CONSTRAINT `users_appoinments_carnet_id_20aad664_fk_users_carnetmodel_name` FOREIGN KEY (`carnet_id`) REFERENCES `users_carnetmodel` (`name`),
  ADD CONSTRAINT `users_appoinments_idService_id_948d52d0_fk_users_ser` FOREIGN KEY (`idService_id`) REFERENCES `users_service` (`idservice`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
