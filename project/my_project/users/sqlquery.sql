-- PRIMERA CONSULTA --

-- Esta consulta devuelve todos los carnet --

SELECT idCarnet FROM users_carnetmodel WHERE name IN (SELECT carnet_id FROM users_appointments)

-- FUNCTIONAL SQL --

SELECT idCarnet FROM users_carnetmodel WHERE name IN (SELECT carnet_id FROM users_appoinments)

-- END PRIMERA CONSULTA --

-- SEGUNDA CONSULTA --

-- Esta consulta devuelve todas las direcciones de correo electrónico --

SELECT email FROM account_emailaddress WHERE user_id IN (SELECT idCarnet FROM users_carnetmodel WHERE name IN (SELECT carnet_id FROM users_appoinments))

-- END SEGUNDA CONSULTA --

-- TERCERA CONSULTA --

-- Esta consulta devuelve todos los datos de los usuarios --

SELECT A.carnet_id, A.date, E.email FROM users_appoinments A, account_emailaddress E, users_carnetmodel C WHERE A.carnet_id = C.name AND C.idCarnet = E.user_id

-- FUNCTIONAL SQL --

SELECT A.carnet_id, A.date, E.email FROM users_appoinments A, account_emailaddress E, users_carnetmodel C WHERE A.carnet_id = C.name AND C.idCarnet = E.user_id

-- FUNCTIONAL SQL WITH NAME --

SELECT CONCAT(U.first_name,' ', U.last_name) AS nombre, A.carnet_id, A.date, E.email FROM auth_user U,users_appoinments A, account_emailaddress E, users_carnetmodel C WHERE A.carnet_id = C.name AND C.idCarnet = E.user_id AND E.user_id = U.id;

-- FUNCTIONAL SQL WITH USER NAME AND SERVICE NAME --

SELECT CONCAT(U.first_name,' ', U.last_name) AS nombre, A.carnet_id, A.date, E.email, S.nameService FROM auth_user U,users_appoinments A, account_emailaddress E, users_carnetmodel C, users_service S WHERE A.carnet_id = C.name AND C.idCarnet = E.user_id AND E.user_id = U.id AND A.idService_id = S.idService