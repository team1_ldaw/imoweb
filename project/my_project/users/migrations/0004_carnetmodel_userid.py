# -*- coding: utf-8 -*-
# Generated by Django 1.11.24 on 2019-11-28 00:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20191104_0411'),
    ]

    operations = [
        migrations.AddField(
            model_name='carnetmodel',
            name='userId',
            field=models.CharField(default=None, max_length=100),
        ),
    ]
