# -*- coding: utf-8 -*-
# Generated by Django 1.11.24 on 2019-10-28 05:13
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Appoinments',
            fields=[
                ('idAppointment', models.AutoField(primary_key=True, serialize=False)),
                ('date', models.DateTimeField()),
                ('status', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='CarnetModel',
            fields=[
                ('name', models.CharField(max_length=100, primary_key=True)),
                ('status', models.IntegerField(default=1)),
                ('idCarnet', models.IntegerField(primary_key=True, serialize=False, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('idService', models.AutoField(primary_key=True, serialize=False)),
                ('nameService', models.CharField(max_length=255, unique=True)),
                ('startTime', models.CharField(max_length=9)),
                ('endTime', models.CharField(max_length=9)),
                ('startDay', models.CharField(max_length=2)),
                ('endDay', models.CharField(max_length=2)),
                ('numOffices', models.IntegerField()),
                ('duration', models.IntegerField(default=None)),
            ],
        ),
        migrations.AddField(
            model_name='appoinments',
            name='carnet',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.CarnetModel'),
        ),
        migrations.AddField(
            model_name='appoinments',
            name='idService',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='users.Service'),
        ),
    ]
