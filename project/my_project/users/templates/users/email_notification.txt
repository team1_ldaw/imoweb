Estimad@:
{{ nombre }}

Te escribimos desde el Instituto Mexicano de Oftalmología para informarte que tienes una cita de {{ nameService }} registrada con el carnet: " {{ carnet_id }} " para la fecha {{ date }} horas.

No es necesario responder, este correo es meramente informativo.

Saludos.

Para más información ingresa a nuestro sitio web https://imoiap.com.mx/