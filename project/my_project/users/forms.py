from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Appoinments
from django.forms import ModelForm

class UserRegistrationForm (UserCreationForm):
    email= forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

class AppointmentForm(ModelForm):
    class Meta:
        model = Appoinments
        fields = ['date', 'status', 'carnet', 'idService']

class FeedbackForm(forms.Form):
    nombre_contacto = forms.CharField(required=True)
    correo_contacto = forms.EmailField(required=True)
    asunto = forms.CharField(required=True)
    comentarios = forms.CharField(widget=forms.Textarea, required=True)

# class FeedbackForm(forms.Form):
#     contact_name = forms.CharField(required=True)
#     contact_email = forms.EmailField(required=True)
#     content = forms.CharField(required=True)