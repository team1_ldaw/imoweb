# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class CarnetModel(models.Model):
	name 		= models.CharField(max_length=100, primary_key=True)
	status 		= models.IntegerField(default=1)
	idCarnet 	= models.IntegerField(unique=True)
	userId 		= models.CharField(max_length=100, default=None)

	def __str__(self):
		return self.name

class Service(models.Model):
	idService 			= models.AutoField(primary_key=True)
	nameService 		= models.CharField(max_length=255, unique=True)
	startTime 			= models.CharField(max_length=9)
	endTime 			= models.CharField(max_length=9)
	startDay 			= models.CharField(max_length=2)
	endDay 				= models.CharField(max_length=2)
	numOffices 			= models.IntegerField()
	duration 			= models.IntegerField(default=None)
	status 				= models.IntegerField(default=1)


	def __str__(self):
		return self.idService


class Appoinments(models.Model):
	idAppointment 		= models.AutoField(primary_key=True)
	date 				= models.DateTimeField()
	status 				= models.IntegerField()
	carnet 				= models.ForeignKey(CarnetModel, on_delete=models.CASCADE)
	idService 			= models.ForeignKey(Service, on_delete=models.CASCADE, default=None)
	
	def __str__(self):
		return self.idAppointment
