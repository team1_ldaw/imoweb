import django_filters
from .models import Appoinments

class AppoinmentsFilter(django_filters.FilterSet):
	class Meta:
		model = Appoinments
		fields = ('idAppointment','date','idService')