# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from .models import CarnetModel, Appoinments, Service
from .forms import UserRegistrationForm
from .forms import AppointmentForm
from .forms import FeedbackForm
from django.core.mail import EmailMessage
from . import forms
import datetime
import time
import pytz
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.core import mail
from django.template import loader
from django.core.mail import EmailMultiAlternatives    
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.template.loader import get_template
from django.utils.http import urlquote_plus
from django.shortcuts import render
from django.db import connection
import pyrebase

firebaseConfig = {
    'apiKey': 'AIzaSyCrki031e5519NiPjC9URf7j0_Upk4-lAY',
    'authDomain': 'iosmovile.firebaseapp.com',
    'databaseURL': 'https://iosmovile.firebaseio.com',
    'projectId': 'iosmovile',
    'storageBucket': 'iosmovile.appspot.com',
    'messagingSenderId': '462892993451',
    'appId': '1:462892993451:web:951c5a8fa6994ef428a0ee',
    'measurementId': 'G-7184J402K0'
  };

firebase = pyrebase.initialize_app(firebaseConfig)
authe = firebase.auth()
database = firebase.database() 

@login_required
def account_mobile(request):
    if request.method == 'POST': #register on firebase
        userEmail = request.POST.get("emailUser", "")
        password = request.POST.get('passwd')
        print(userEmail)
        print(password)
        try:
            user = authe.create_user_with_email_and_password(userEmail,password)
            flag = 1
        except:
            flag = 2
        #uId = user['localId']
        #data = {"firstname":"Prueba", "lastname":"Preuwba2"}
        #database.child("users").child(uId).child("details").set(data)
        context = {'flag':flag}
        return render (request, 'users/firebase.html', context)
    else: #GET
        return render (request, 'users/firebase.html', {}) 

def send_mail(request):
    if request.method == 'POST':
        flag = 2
        utc_now = pytz.utc.localize(datetime.datetime.utcnow())
        currDate = utc_now.astimezone(pytz.timezone("America/Mexico_City"))
        currDate = currDate.strftime("%Y-%m-%d %H:%M:%S")
        carnet = request.POST.get('carnet')
        appointments = Appoinments.objects.filter(carnet_id=carnet)
        context = {'appointments': appointments, 'currDate' : currDate, 'flag': flag}
        
        subject = 'Solicitud de Citas - IMO IAP'

        umail = request.user.email
        #print(umail)

        from_email = 'info@imoiap.com'
        to = umail

        html = get_template('users/appointmentsMail.html')

        # Render the template with the data
        content_html = html.render(context)
        content_text = '' 

        # Send mail
        msg = EmailMultiAlternatives(subject, content_html, from_email, [to])
        msg.attach_alternative(content_html, "text/html")
        msg.send()
            
        return redirect('appointments')

def smP(request):
    if request.method == 'POST':
        #Render the view
        flag = 2
        utc_now = pytz.utc.localize(datetime.datetime.utcnow())
        currDate = utc_now.astimezone(pytz.timezone("America/Mexico_City"))
        currDate = currDate.strftime("%Y-%m-%d %H:%M:%S")
        carnet = request.POST.get('carnet')
        print(carnet)
        appointments = Appoinments.objects.filter(carnet_id=carnet)
        context = {'appointments': appointments, 'currDate' : currDate, 'flag': flag}
        
        subject = 'Solicitud de Citas - IMO IAP'

        from_email = 'info@imoiap.com'
        to = 'gleasontrash@gmail.com'

        html = get_template('users/appointmentsPastMail.html')

        # Render the template with the data
        content_html = html.render(context)
        content_text = '' 

        # Send mail
        msg = EmailMultiAlternatives(subject, content_html, from_email, [to])
        msg.attach_alternative(content_html, "text/html")
        msg.send()
            
        return redirect('appointmentsPast')

def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        carnet=request.POST.get("carnet")
        if CarnetModel.objects.filter(name=carnet).exists():
            request.session['carnet'] = carnet
            uID = CarnetModel.objects.get(name=carnet)
            if uID.userId == None: #Empty carnet
                if form.is_valid():
                    form.save()
                    username = form.cleaned_data.get('username')
                    CarnetModel.objects.filter(name=carnet).update(userId=username)
                    messages.success(request, 'Tu cuenta ha sido creada %s' % username)
                    return redirect('login')
                else:
                    flag = 2
                    form = UserRegistrationForm()
                    return render (request, 'users/register.html', {'form':form, 'flag': flag})
            else: #Used carnet
                flag = 3
                form = UserRegistrationForm()
                return render (request, 'users/register.html', {'form':form, 'flag': flag})
        else:
            flag = 4
            form = UserRegistrationForm()
            return render (request, 'users/register.html', {'form':form, 'flag': flag})
    else:
        car = request.GET.get('car')
        if car:
            flag = 1
            form = UserRegistrationForm()
            return render (request, 'users/register.html', {'form':form, 'car':car, 'flag': flag})
        else:
            flag = 0
            form = UserRegistrationForm()
            return render (request, 'users/register.html', {'form':form, 'flag': flag})

def carnet(request):
    if request.method == 'POST':
        carnet=request.POST.get("carnet")
        request.session['carnet'] = carnet
        #print(carnet)
        if (CarnetModel.objects.filter(name=carnet).exists()):
            car = CarnetModel.objects.get(name=carnet)
            if car.status == 0:
                messages.warning(request, 'Su carnet es inválido')
                return render (request, 'users/carnet.html', {})
            elif car.status == 1:
                #messages.success(request, 'Su carnet: %s' % carnet)
                return redirect('login')
        else:
            messages.warning(request, 'Su carnet es inválido')
    return render (request, 'users/carnet.html', {})

def admin_add_service(request):
    if request.user.is_staff == 0:
        return redirect('/')
    elif request.user.is_staff == 1:
        if request.method == 'GET':
            return render (request, 'users/admin_add_service.html', {})
        else: #post
            nameService = request.POST.get('nameService')
            startTime = request.POST.get('startTime')
            startTime = startTime +':00'
            endTime = request.POST.get('endTime')
            endTime = endTime +':00'
            startDay = request.POST.get('startDay')
            endDay = request.POST.get('endDay')
            numOffices = request.POST.get('numOffices')
            duration = request.POST.get('duration')
            if Service.objects.filter(nameService=nameService).exists():
                flag = 1
            else:
                flag = 2
                servNew = Service(nameService=nameService, startTime=startTime, endTime=endTime, startDay=startDay, endDay=endDay, numOffices=numOffices, duration=duration)
                servNew.save()
            context = {'flag': flag}
            return render (request, 'users/admin_add_service.html', context)

@csrf_exempt
def appointment_view(request):
    if not request.user.is_authenticated:
        return redirect('carnet')

    context = {}

    if request.POST:#cancel apt
        flag = 1
        utc_now = pytz.utc.localize(datetime.datetime.utcnow())
        currDate = utc_now.astimezone(pytz.timezone("America/Mexico_City"))
        content = request.POST.get('idCita')
        idSes = request.session['carnet']
        num = Appoinments.objects.filter(idAppointment=content, carnet_id=idSes).update(status=0)
        #Continue with normal data retrieval
        appointments = Appoinments.objects.all()
        utc_now = pytz.utc.localize(datetime.datetime.utcnow())
        currDate = utc_now.astimezone(pytz.timezone("America/Mexico_City"))
        currDate = currDate.strftime("%Y-%m-%d %H:%M:%S")
        services = Service.objects.all()
        context= {'appointments': appointments, 'currDate' : currDate, 'num': num, 'flag': flag, 'services' : services}

        #(msg)
        return HttpResponse('')

    else: #get
        flag = 0
        search = request.GET.get('filter')
        if search is not None:
            if search == '':
                appointments = Appoinments.objects.all()
            else:
                appointments = Appoinments.objects.filter(idService_id=search)
            print(search)
        else:
            appointments = Appoinments.objects.all()
        services = Service.objects.all()
        utc_now = pytz.utc.localize(datetime.datetime.utcnow())
        currDate = utc_now.astimezone(pytz.timezone("America/Mexico_City"))
        currDate = currDate.strftime("%Y-%m-%d %H:%M:%S")
        context= {'appointments': appointments, 'currDate' : currDate, 'flag': flag, 'services' : services}
        return render(request, 'users/appointments.html', context)

@csrf_exempt
def admin_users(request):
    if request.user.is_staff == 0:
        return redirect('/')
    elif request.user.is_staff == 1:
        if request.method == 'GET':
            users = User.objects.all()
            context= {'users': users}
            return render(request, 'users/admin_users.html', context)
        else:
            usrId = request.POST.get('idUsuario')
            if usrId is not None:
                user = User.objects.get(id=usrId)
                if user.is_staff == 0:
                    User.objects.filter(id=usrId).update(is_staff=1)
                else:
                    User.objects.filter(id=usrId).update(is_staff=0)
                return HttpResponse('')
            usrAdminId = request.POST.get('idUsrAdmin')
            if usrAdminId is not None:
                user = User.objects.get(id=usrAdminId)
                if user.is_active == 0:
                    User.objects.filter(id=usrAdminId).update(is_active=1)
                else:
                    User.objects.filter(id=usrAdminId).update(is_active=0)
                return HttpResponse('')

def admin_modify_service(request):
    if request.user.is_staff == 0:
        return redirect('/')
    elif request.user.is_staff == 1:
        if request.method == 'GET':
            serviceId = request.GET.get('idService')
            print(serviceId)
            serv = Service.objects.get(idService=serviceId)
            context = {'serv':serv}
            return render(request, 'users/admin_modify_service.html', context)
        else:
            idService = request.POST.get('idService')
            nameService = request.POST.get('nameService')
            startTime = request.POST.get('startTime')
            endTime = request.POST.get('endTime')
            startDay = request.POST.get('startDay')
            endDay = request.POST.get('endDay')
            numOffices = request.POST.get('numOffices')
            duration = request.POST.get('duration')
            print(idService)
            print(nameService)
            print(startTime)
            print(endTime)
            print(startDay)#good
            print(endDay)#good
            print(numOffices)
            print(duration)

            if idService is not None:
                if nameService is not None and nameService is not "":
                    if Service.objects.filter(nameService=nameService).exists():
                        flag = 1
                    else:
                        Service.objects.filter(idService=idService).update(nameService=nameService)
                if startTime is not None and startTime is not "":
                    startTime = startTime +':00'
                    Service.objects.filter(idService=idService).update(startTime=startTime)
                    pass
                if endTime is not None and endTime is not "":
                    endTime = endTime +':00'
                    Service.objects.filter(idService=idService).update(endTime=endTime)
                    pass
                if startDay is not None and startDay is not "":
                    Service.objects.filter(idService=idService).update(startDay=startDay)
                    pass
                if endDay is not None and endDay is not "":
                    Service.objects.filter(idService=idService).update(endDay=endDay)
                    pass
                if numOffices is not None and numOffices is not "":
                    Service.objects.filter(idService=idService).update(numOffices=numOffices)
                    pass
                if duration is not None and duration is not "":
                    Service.objects.filter(idService=idService).update(duration=duration)
                    pass
                flag = 2
                serv = Service.objects.get(idService=idService)
                context = {'serv':serv, 'flag':flag}
            else:
                flag = 3
                context = {'flag':flag}
            return render(request, 'users/admin_modify_service.html', context)


@csrf_exempt
def admin_carnets(request):
    if request.user.is_staff == 0:
        return redirect('/')
    elif request.user.is_staff == 1:
        if request.method == 'GET':
            carnets = CarnetModel.objects.all()
            context = {'carnets': carnets}
            #print(carnets)
            msg = 0
            return render(request, 'users/admin_carnets.html', context)
        else:
            carnet = request.POST.get('idCarnet')
            if carnet is not None:
                print(carnet)
                car = CarnetModel.objects.get(idCarnet=carnet)
                if car.status == 0:
                    CarnetModel.objects.filter(idCarnet=carnet).update(status=1)
                else:
                    CarnetModel.objects.filter(idCarnet=carnet).update(status=0)
                return HttpResponse('')
            new = request.POST.get('filter')
            if new is not None:
                if CarnetModel.objects.filter(name=new).exists():
                    msg = 1
                    carnets = CarnetModel.objects.all()
                    context= {'carnets': carnets, 'msg':msg}
                else:
                    msg = 2
                    carNew = CarnetModel(name=new)
                    carNew.save()
                    carnets = CarnetModel.objects.all()
                    context= {'carnets': carnets, 'msg':msg}
                return render(request, 'users/admin_carnets.html', context)     

@csrf_exempt
def admin_services(request):
    if request.user.is_staff == 0:
        return redirect('/')
    elif request.user.is_staff == 1:
        if request.method == 'GET':
            services = Service.objects.all()
            context= {'services': services}
            return render(request, 'users/admin_services.html', context)
        else:
            idServicio = request.POST.get('idServicio')
            if idServicio is not None:
                serv = Service.objects.get(idService=idServicio)
                if serv.status == 0:
                    Service.objects.filter(idService=idServicio).update(status=1)
                else:
                    Service.objects.filter(idService=idServicio).update(status=0)
                return HttpResponse('')


def appointmentPast_view(request):
    if not request.user.is_authenticated:
        return redirect('carnet')
    context = {}
    if request.method == 'GET':#cancel apt
        flag = 0
        search = request.GET.get('filter')
        if search is not None:
            if search == '':
                appointments = Appoinments.objects.all()
            else:
                appointments = Appoinments.objects.filter(idService_id=search)
            print(search)
        else:
            appointments = Appoinments.objects.all()
        services = Service.objects.all()
        utc_now = pytz.utc.localize(datetime.datetime.utcnow())
        currDate = utc_now.astimezone(pytz.timezone("America/Mexico_City"))
        currDate = currDate.strftime("%Y-%m-%d %H:%M:%S")
        context= {'appointments': appointments, 'currDate' : currDate, 'flag': flag, 'services' : services}
        return render(request, 'users/appointmentsPast.html', context)

#decorator
@login_required
def profile (request):
    if request.POST:#change username
        flag = 1
        username = request.POST.get('actusername')
        newusername = request.POST.get('username')
        if User.objects.filter(username=newusername).exists():
            flag = 2
            context={'flag': flag, 'uname': username}
            return render(request, 'users/profile.html', context)
        else:
            user = User.objects.get(username = username)
            user.username = newusername
            user.save()
            context={'flag': flag, 'uname' : newusername}
            return render(request, 'users/profile.html', context)
    else: #get request
        flag = 0
        uname = request.user.username
        context = {'uname': uname}
        return render(request, 'users/profile.html', context)

@login_required
def add_appointment_form_submission(request):
    mssg = None
    err = None
    link = None
    services = Service.objects.all()
    variable = []
    fulldateString = None
    fullnumOffices = None
    availableError = None
    dateString = None
    date = request.POST.get('date', None)
    #carnet = request.POST.get('carnet', None)
    idService = request.POST.get('idService', None)
    if request.method == 'POST':
        raw_appointment = forms.AppointmentForm(request.POST)
        if raw_appointment.is_valid():
            appointment = raw_appointment.save(commit=False)
            appointment.user = request.user
            #print(appointment.date)
            #print(appointment.idService)
            registeredAppointments = Appoinments.objects.filter(date=appointment.date, idService=idService)
            #print(len(registeredAppointments))
            #print(idService)
            availableAppointments = Service.objects.get(idService__iexact=idService)
            d_date = datetime.datetime.now()
            reg_format_date = d_date.strftime("%Y-%m-%d %H:%M:%S")
            utc_now = pytz.utc.localize(datetime.datetime.utcnow())
            pst_now = utc_now.astimezone(pytz.timezone("America/Mexico_City"))
            nameCalendar = availableAppointments.nameService
            hourStep = availableAppointments.duration or 0 # This 0 is assigned to prevent errors from NULL values
            nameLink = urlquote_plus(('Cita médica para: ' + nameCalendar + ' en IMO'))
            startDate = appointment.date
            endDate = appointment.date + datetime.timedelta(hours=hourStep)
            tfmt = '%Y%m%dT%H%M%S'
            dates = '%s%s%s' % (startDate.strftime(tfmt), '%2F', endDate.strftime(tfmt))
            website = 'https://imoiap.com.mx/'
            location = 'Instituto Mexicano de Oftalmología IAP'
            details = urlquote_plus(('Registro de cita médica para ' + nameCalendar + ', en el día y hora ' + startDate.strftime("%d/%m/%Y, %H:%M:%S") + ', en el ' + location + ' con duración de (' + str(hourStep) + ') horas.'))
            calendarLink = ('http://www.google.com/calendar/event?action=TEMPLATE&' +
                'text=' + nameLink + '&' +
                'dates=' + dates + '&' +
                'sprop=website:' + website + '&details=' + details + '&location=' + urlquote_plus(location) + '&trp=false')
            if len(registeredAppointments)<availableAppointments.numOffices:
                if appointment.date > pst_now:
                # if (appointment.date).date() >= pst_now.date() and (appointment.date).hour > pst_now.hour:
                    try:
                        appointment.save()
                        mssg = "Éxito al registrar una cita"
                        link = calendarLink
                        appointment_send_email(request, appointment, nameCalendar)
                        #email_appointment_notification()
                        # cursor = connection.cursor()
                        # cursor.execute("SELECT CONCAT(U.first_name,' ', U.last_name) AS nombre, A.carnet_id, A.date, E.email FROM auth_user U,users_appoinments A, account_emailaddress E, users_carnetmodel C WHERE A.carnet_id = C.name AND C.idCarnet = E.user_id AND E.user_id = U.id")
                        # element = cursor.fetchall()
                        # for cursor in element:
                        #     print(cursor[3])
                    except:
                        err = str(raw_appointment.errors)
                else:
                    err="No se pueden agendar citas en fechas anteriores a la fecha actual." 
            else:
                err="Este horario está lleno, por favor intente agendar en otro horario."
                fullnumOffices = True
                # Appointment date
                appDate = appointment.date
                # Appointment date as a string
                dateString = appDate.strftime('%d %b %Y')
                fulldateString = appDate.strftime('%d %b %Y' + ' a las ' '%H:%M' + ' horas')
                # Service object
                serviceObject = Service.objects.get(idService__iexact=idService)
                # Service object startTime as String
                servicestString = serviceObject.startTime
                # Converting string to datetime object in order to manipulate data
                servicestartdateTime = datetime.datetime.strptime(servicestString, '%H:%M:%S')
                # Passing service startTime data to current datetime as int values
                startHour = servicestartdateTime.strftime('%H')
                startMinute = servicestartdateTime.strftime('%M')
                servicestartTime = appDate.replace(hour=int(startHour), minute=int(startMinute)) # Parsing values
                # Service object endTime as String
                serviceendString = serviceObject.endTime
                # Converting string to datetime object in order to manipulate data
                serviceenddateTime = datetime.datetime.strptime(serviceendString, '%H:%M:%S')
                # Passing service endTime data to current datetime as int values
                endHour = serviceenddateTime.strftime('%H')
                endMinute = serviceenddateTime.strftime('%M')
                serviceendTime = appDate.replace(hour=int(endHour), minute=int(endMinute)) # Parsing values
                # Counter for increment of hours
                count = 1
                while servicestartTime < serviceendTime:
                    savedAppointments = Appoinments.objects.filter(date=servicestartTime, idService=idService)
                    avAppointments = Service.objects.get(idService__iexact=idService)
                    if len(savedAppointments)<avAppointments.numOffices:
                        if servicestartTime > pst_now:
                            variable.append(servicestartTime.strftime('%H:%M'))
                            #print(servicestartTime.strftime('%H:%M'))
                    servicestartTime = servicestartTime + datetime.timedelta(hours=count)
                #print(variable)
                # In case there are no available dates for scheduling
                if not variable:
                    availableError = 'No hay horarios disponibles el día de hoy. Intente seleccionar otra fecha.'
        else:
            err = str(raw_appointment.errors)
    return render(request, 'users/create_appointment.html',{
        'message': mssg,
        'error': err,
        'link': link,
        'services': services,
        'variable': variable,
        'dateString': dateString,
        'fulldateString': fulldateString,
        'fullnumOffices': fullnumOffices,
        'availableError': availableError,
        #'appointments': Appoinments.objects.all()
    })

def appointment_send_email(request, appointment, nameCalendar):
    print('Sending email')
    emailApp = request.user.email
    if emailApp:
        asunto = 'Cita médica para: ' + nameCalendar
        nombre = appointment.user
        carnet_id = request.POST.get("carnet")
        date = appointment.date
        email = emailApp
        nameService = nameCalendar
        template = get_template('users/email_notification.txt')
        content = {
            'nombre' : nombre,
            'nameService' : nameService,
            'carnet_id' : carnet_id,
            'date' : date,
        }
        content = template.render(content)
        email = EmailMessage(
            asunto,
            content,
            'IMO Web' + '',
            [email],
            headers = {'Reply to': email}
        )
        try:
            email.send()
            print('True')
        except:
            print('False')
    else:
        print('Errror')


@login_required
def share(request):
    mssg = None
    err = None
    if request.method == 'GET':
        return render(request, 'users/share.html', {})
    else:
        asunto = '¡Conoce los servicios del Instituto Mexicano de Oftalmología'
        nombre_contacto = request.POST.get('nombre_contacto')
        correo_contacto = request.POST.get('correo_contacto')
        comentarios = request.POST.get('comentarios')
        template = get_template('users/share_form.txt')
        content = {
            'nombre_contacto' : request.POST.get('nombre_contacto'),
            'comentarios' : request.POST.get('comentarios'),
        }
        content = template.render(content)
        email = EmailMessage(
            asunto,
            content,
            'IMO Web' + '',
            [correo_contacto],
            headers = {'Reply to': correo_contacto}
        )
        try:
            email.send()
            mssg = '¡El usuario ha sido invitado!'
        except:
            err = '¡Error al enviar la invitación, favor de intentar de nuevo!'
    return render(request, "users/share.html", {
        'message': mssg,
        'error': err,
    })

@login_required
def feedback_email_form_submission(request):
    mssg = None
    err = None
    if request.method == 'GET':
        form = FeedbackForm()
    else:
        form = FeedbackForm(request.POST)
        if form.is_valid():
            asunto = request.POST.get('asunto')
            nombre_contacto = request.POST.get('nombre_contacto')
            correo_contacto = request.POST.get('correo_contacto')
            comentarios = request.POST.get('comentarios')
            template = get_template('users/feedback_form.txt')
            content = {
                'nombre_contacto' : request.POST.get('nombre_contacto'),
                'correo_contacto' : request.POST.get('correo_contacto'),
                'comentarios' : request.POST.get('comentarios'),
            }
            content = template.render(content)
            email = EmailMessage(
                asunto,
                content,
                'IMO Web' + '',
                ['a01703171@itesm.mx'],
                #['informacion@imoiap.edu.mx'],
                headers = {'Reply to': correo_contacto}
            )
        try:
            email.send()
            mssg = '¡Gracias por tus comentarios!'
            email_appointment_notification()
        except:
            err = '¡Por favor llene todos los campos!'
    return render(request, "users/send_feedback.html", {
        'message': mssg,
        'error': err,
    })
    
@login_required
def add_appointment_form_submission_reschedule(request):
    mssg = None
    err = None
    services = Service.objects.all()
    date = request.POST.get('date', None)
    idService = request.POST.get('idService', None)
    return render(request, 'users/create_appointment_rs.html',{
        'message': mssg,
        'error': err,
        'services': services,
        #'appointments': Appoinments.objects.all()
    })

def email_appointment_notification():
    cursor = connection.cursor()
    try:
        cursor.execute("SELECT CONCAT(U.first_name,' ', U.last_name) AS nombre, A.carnet_id, A.date, E.email, S.nameService FROM auth_user U,users_appoinments A, account_emailaddress E, users_carnetmodel C, users_service S WHERE A.carnet_id = C.name AND C.idCarnet = E.user_id AND E.user_id = U.id AND A.idService_id = S.idService")
    finally:
        cursor.close()
    mail_notification_logic(cursor)
    
def mail_notification_logic(cursor):
    datetimetest = datetime.datetime.now()
    for obj in cursor.fetchall():
        if obj[2] > datetimetest:
            asunto = 'Cita médica para: ' + obj[4]
            nombre = obj[0]
            carnet_id = obj[1]
            date = obj[2]
            email = obj[3]
            nameService = obj[4]
            template = get_template('users/email_notification.txt')
            content = {
                'nombre' : nombre,
                'nameService' : nameService,
                'carnet_id' : carnet_id,
                'date' : date,
            }
            content = template.render(content)
            email = EmailMessage(
                asunto,
                content,
                'IMO Web' + '',
                [email],
                headers = {'Reply to': email}
            )
            try:
                email.send()
                print('True')
            except:
                print('False')
        else:
            print('Nah')

def get_service_data(request, id):
    service = Service.objects.get(idService=id)
    print (service)

def validate_service_date(request):
    idService = request.GET.get('idService', None)
    serviceQuery = Service.objects.get(idService__iexact=idService)
    data = {
        'is_available': Service.objects.filter(idService__iexact=idService).exists(),
        'startTime': serviceQuery.startTime,
        'endTime': serviceQuery.endTime,
        'startDay': serviceQuery.startDay,
        'endDay': serviceQuery.endDay,
        'duration': serviceQuery.duration,
        #'is_available': Service.objects.filter(idService__iexact=idService).exists()
        #'is_available': Service.objects.filter()
    }
    return JsonResponse(data)

def staff_imo(request):
    return render(request, 'users/staff_imo.html', {'title': 'Mi cuenta'})

def preguntas_frecuentes(request):
    return render(request, 'users/faq_section.html', {'title': 'Preguntas Frecuentes'})

def aviso_privacidad(request):
    return render(request, 'users/aviso_privacidad.html', {'title': 'Aviso de Privacidad'})
