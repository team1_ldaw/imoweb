from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='schedueling-home'),
    url(r'^about/$', views.about, name='schedueling-about'),
    url(r'^manual/$', views.manual, name='schedueling-manual'),
    url(r'^contacto/$', views.contacto, name='schedueling-contacto'),
]
