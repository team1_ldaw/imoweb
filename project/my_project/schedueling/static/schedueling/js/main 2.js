$(document).ready(function(){
apply_global_theme();
})

/*For dark mode*/
var apply_global_theme = function(){
    var bg_color = localStorage.getItem("global_theme") || '#ffffff';
    $("body").css("background", bg_color);
}

function changeBackground(){
	var bg_color = localStorage.getItem("global_theme") || '#ffffff';
    bg_color = bg_color == '#ffffff' ? '#807F7F7F' : '#ffffff';
	localStorage.setItem("global_theme", bg_color);
	apply_global_theme();
}

function cancelar(idCita){
	//Print imprime la pag
	console.log(idCita);
    var data = {idCita: idCita};
	$.post('/appointments/', data)
	.done(function(data){
		$('#'+idCita).text('Cancelada');
		$('#'+idCita).attr("disabled", true);
		$('#'+idCita).addClass("disabled");
		$('#'+idCita).removeClass("btn btn-danger");
		$('#'+idCita).addClass("btn btn-light");
		$('#resc'+idCita).text('Cancelada');
		$('#resc'+idCita).attr("disabled", true);
		$('#resc'+idCita).addClass("disabled");
		$('#resc'+idCita).removeClass("btn btn-primary");
		$('#resc'+idCita).addClass("btn btn-light");
	});
}

function desactivarCarnet(idCarnet){
	//Print imprime la pag
	console.log(idCarnet);
    var data = {idCarnet: idCarnet};
	$.post('/admin_carnets/', data)
	.done(function(data){
		$('#'+idCarnet).text('Activar');
		$('#'+idCarnet).removeClass("btn btn-outline-danger");
		$('#'+idCarnet).addClass("btn btn-outline-success");
	});
}

function activarCarnet(idCarnet){
	//Print imprime la pag
	console.log(idCarnet);
    var data = {idCarnet: idCarnet};
	$.post('/admin_carnets/', data)
	.done(function(data){
		$('#'+idCarnet).text('Desactivar');
		$('#'+idCarnet).removeClass("btn btn-outline-success");
		$('#'+idCarnet).addClass("btn btn-outline-danger");
	});
}

function activarAdmin(idUsuario){
	//Print imprime la pag
	console.log(idUsuario);
    var data = {idUsuario: idUsuario};
	$.post('/admin_users/', data)
	.done(function(data){
		$('#'+idUsuario).text('×');
		$('#'+idUsuario).removeClass("btn btn-outline-success");
		$('#'+idUsuario).addClass("btn btn-outline-danger");
	});
}

function removerAdmin(idUsuario){
	//Print imprime la pag
	console.log(idUsuario);
    var data = {idUsuario: idUsuario};
	$.post('/admin_users/', data)
	.done(function(data){
		$('#'+idUsuario).text('✔');
		$('#'+idUsuario).removeClass("btn btn-outline-danger");
		$('#'+idUsuario).addClass("btn btn-outline-success");
	});
}

function activarUsr(idUsuario){
	//Print imprime la pag
	console.log(idUsuario);
    var data = {idUsrAdmin: idUsuario};
	$.post('/admin_users/', data)
	.done(function(data){
		$('#act'+idUsuario).text('×');
		$('#act'+idUsuario).removeClass("btn btn-outline-success");
		$('#act'+idUsuario).addClass("btn btn-outline-danger");
	});
}

function desactivarUsr(idUsuario){
	//Print imprime la pag
	console.log(idUsuario);
    var data = {idUsrAdmin: idUsuario};
	$.post('/admin_users/', data)
	.done(function(data){
		$('#act'+idUsuario).text('✔');
		$('#act'+idUsuario).removeClass("btn btn-outline-danger");
		$('#act'+idUsuario).addClass("btn btn-outline-success");
	});
}

function activarServicio(idServicio){
	//Print imprime la pag
	console.log(idServicio);
    var data = {idServicio: idServicio};
	$.post('/admin_services/', data)
	.done(function(data){
		$('#'+idServicio).text('Desactivar');
		$('#'+idServicio).removeClass("btn btn-outline-success");
		$('#'+idServicio).addClass("btn btn-outline-danger");
	});
}

function desactivarServicio(idServicio){
	//Print imprime la pag
	console.log(idServicio);
    var data = {idServicio: idServicio};
	$.post('/admin_services/', data)
	.done(function(data){
		$('#'+idServicio).text('Activar');
		$('#'+idServicio).removeClass("btn btn-outline-danger");
		$('#'+idServicio).addClass("btn btn-outline-success");
	});
}

function reagendar(idCita){
	//Print imprime la pag
	console.log(idCita);
	var csrftoken = $('input[name = "csrfmiddlewaretoken"]').val();
	/*Cuando acabe recibe el context*/
	$.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Csrf-Token', csrftoken);
        }
    });
    var data = {idCita: idCita};
    console.log(csrftoken);
	$.post('/appointments/', data)
	.done(function(data){
		window.location.href = '/add_appointment_form_submission_reschedule';
	});

}

$.fn.datetimepicker.defaults = {
  maskInput: true,           // disables the text input mask
  pickDate: true,            // disables the date picker
  pickTime: true,            // disables de time picker
  pick12HourFormat: false,   // enables the 12-hour format time picker
  pickSeconds: true,         // disables seconds in the time picker
  startDate: -Infinity,      // set a minimum date
  endDate: Infinity          // set a maximum date
};