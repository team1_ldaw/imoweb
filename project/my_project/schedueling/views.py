# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
#user creation form
from django.contrib.auth.decorators import login_required
from users.models import CarnetModel, Appoinments
from django.contrib.auth.models import User
import datetime
import calendar
from django import template

def home(request):
	today = datetime.date.today()
	months = ['zero','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
	current_month = months[today.month]
	current_year = today.year
	now = datetime.datetime.now()
	n = calendar.monthrange(now.year, now.month)[1] + 1
	context = {}
	appointments = Appoinments.objects.filter(date__gte=datetime.date(2019, 10, 1),date__lte=datetime.date(2019, 10, 31))
	dates = Appoinments.objects.values_list('date')
	context= {'appointments': appointments, 'month':current_month, 'year':current_year, 'n':range(n), 'dates':dates}
	return render(request, 'schedueling/home.html', context)

def about(request):
    return render(request, 'schedueling/about.html', {'title': 'About'})
    
def manual(request):
    return render(request, 'schedueling/manual.html', {'title': 'Manual'})

def contacto(request):
    return render(request, 'schedueling/contacto.html', {'title': 'Contacto'})

def account(request):
    return render(request, 'schedueling/account.html', {'title': 'Mi cuenta'})
# Create your views here.
