"""my_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib.auth.views import logout
from django.conf import settings
from django.conf import settings
from django.conf.urls.static import static
from allauth.account import views as acc_views
from users import views as users_views
#here we import all the specific views
from users.views import (
  carnet,
  profile,
  appointment_view,
  appointmentPast_view,
  send_mail,
  smP,
  admin_users,
  admin_carnets,
  admin_services,
  admin_add_service,
  admin_modify_service,
  account_mobile,
  share,
  )

urlpatterns = [
               url(r'admin/', admin.site.urls),
               url(r'^carnet/', carnet, name='carnet'),
               url(r'^login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
               url(r'^register/', users_views.register, name='register'),
               url(r'^appointments/', appointment_view, name='appointments'),
               url(r'^send_mail/', send_mail, name='send_mail'),
               url(r'^smP/', smP, name='smP'),
               url(r'^share/', share, name='share'),
               url(r'^admin_users/', admin_users, name='admin_users'),
               url(r'^admin_carnets/', admin_carnets, name='admin_carnets'),
               url(r'^admin_services/', admin_services, name='admin_services'),
               url(r'^admin_add_service/', admin_add_service, name='admin_add_service'),
               url(r'^admin_modify_service/', admin_modify_service, name='admin_modify_service'),
               #url(r'^register_appointment/', users_views.createappointment_view, name='create_appointment'),
               url(r'^add_appointment_form_submission/$', users_views.add_appointment_form_submission, name='add_appointment_form_submission'),
               url(r'^add_appointment_form_submission_reschedule/$', users_views.add_appointment_form_submission_reschedule, name='add_appointment_form_submission_reschedule'),
               #url('service_data/<int:id>/', users_views.get_service_data),
               url(r'^ajax/validate_service_date/$', users_views.validate_service_date, name='validate_service_date'),
               #url(r'^send_feedback/$', users_views.feedback_email_form_submission, name='send_feedback'),
               url(r'^feedback_email_form_submission/$', users_views.feedback_email_form_submission, name='feedback_email_form_submission'),
               #url(r'^feedback_success/$', users_views.feedback_email_form_success, name='feedback_success'),                        
               url(r'^appointmentsPast/', appointmentPast_view, name='appointmentsPast'),
               url(r'^profile/', users_views.profile, name='profile'),
               url(r'^emails/', acc_views.email, name='account_email'),
               url(r'^account_mobile/', account_mobile, name='account_mobile'),
               url(r'^staff_imo/', users_views.staff_imo, name='staff_imo'),
               url(r'^preguntas_frecuentes/', users_views.preguntas_frecuentes, name='preguntas_frecuentes'),
               url(r'^aviso_privacidad/', users_views.aviso_privacidad, name='aviso_privacidad'),
               url(r'^change-password/', acc_views.password_change, name='account_password_change'),
               url(r'^reset-password/', acc_views.password_reset, name='account_password_reset'),
               url(r'^logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
               url('', include('schedueling.urls')),
               url(r'^', include('django.contrib.auth.urls')),
               url(r'^oauth/', include('social_django.urls', namespace="social")),
               url(r'^auth/', include('social_django.urls', namespace="social")),
               url('logout/', logout, {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
               url('accounts/', include('allauth.urls'))
] +static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)



