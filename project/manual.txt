Para instalar:
Python 2.7.10
pip install Django==1.11.12
pip install django --upgrade

virtualenv project
cd project
django-admin.py startproject my_project
cd my_project
python manage.py startapp login
python manage.py runserver

python manage.py migrate
python manage.py createsuperuser
python manage.py shell

Login template:
https://github.com/rahmato/free-materialize-material-design-admin-template
https://pixinvent.com/materialize-material-design-admin-template/html/ltr/vertical-modern-menu-template/user-login.html
